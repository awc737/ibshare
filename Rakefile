# frozen_string_literal: true
# Add your own tasks in files placed in lib/tasks ending in .rake, for example
# lib/tasks/capistrano.rake, and they will automatically be available to Rake.

require File.expand_path('../config/application', __FILE__)

task :default => [:style, :tests]

task :style => ['style:server', 'style:client']
task :tests => ['spec:server', 'spec:client']

namespace :spec do
  task :server => [:spec]

  task :client do
    sh 'npm test'
  end
end

namespace :style do
  task :server do
    sh 'rubocop'
  end

  task :client do
    sh 'npm run lint'
  end
end

# Do this last so that our tasks and dependencies get defined before others.
# In particular, we want our `:style` tasks to run before `:spec`, but RSpec
# adds a dependency from `:default` to `:spec` automatically.  If that happens
# before we define our tasks, then RSpec will run before the `:style` tasks.
Rails.application.load_tasks
