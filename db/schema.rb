# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160525211642) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "offers", force: :cascade do |t|
    t.decimal  "amount"
    t.integer  "user_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "offers", ["project_id"], name: "index_offers_on_project_id", using: :btree
  add_index "offers", ["user_id"], name: "index_offers_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "legal_entity_name"
    t.string   "location"
    t.string   "title"
    t.decimal  "number_of_acres"
    t.integer  "number_of_buildings"
    t.integer  "number_of_units"
    t.decimal  "total_debt_required"
    t.decimal  "total_investment_required"
    t.string   "type_of_development"
    t.decimal  "minimum_investment"
    t.decimal  "projected_gross_profit"
    t.integer  "estimated_start_quarter"
    t.integer  "estimated_start_year"
    t.decimal  "projected_land_costs"
    t.decimal  "projected_hard_costs"
    t.decimal  "projected_soft_costs"
    t.text     "summary"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "investment_term"
    t.decimal  "annual_roi"
    t.text     "property_details"
    t.text     "financial_summary"
    t.text     "investment_offering"
    t.text     "market_summary"
  end

  create_table "team_members", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.text     "bio"
    t.integer  "project_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "team_members", ["project_id"], name: "index_team_members_on_project_id", using: :btree

  create_table "uploads", force: :cascade do |t|
    t.string   "type"
    t.string   "title"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.integer  "project_id"
    t.integer  "position",                default: 0
    t.boolean  "featured",                default: false
  end

  add_index "uploads", ["project_id"], name: "index_uploads_on_project_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "offers", "projects"
  add_foreign_key "offers", "users"
  add_foreign_key "team_members", "projects"
  add_foreign_key "uploads", "projects"
end
