# Create default users
# user@infobrij.com
# user1@infobrij.com
# password: password
users = FactoryGirl.create_pair(:user)

# convert the first user to an admin
users.first.update_attribute :admin, true

FactoryGirl.create_list :project_fully_loaded, 10,
  media_count: 6,
  team_member_count: 4
