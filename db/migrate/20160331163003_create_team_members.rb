class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.string :name
      t.string :title
      t.text :bio
      t.references :project, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
