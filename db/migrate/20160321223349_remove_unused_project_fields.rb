class RemoveUnusedProjectFields < ActiveRecord::Migration
  def change
    change_table :projects do |t|
      t.remove :funded_so_far, :projected_value, :term
      t.rename :project_pitch, :pitch
    end
  end
end
