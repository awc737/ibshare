class RemoveDocumentsFromProjects < ActiveRecord::Migration
  def self.up
    remove_attachment :projects, :document
    add_attachment :uploads, :attachment
  end

  def self.down
    add_attachment :projects, :document
    remove_attachment :uploads, :attachment
  end
end
