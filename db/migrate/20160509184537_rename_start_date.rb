class RenameStartDate < ActiveRecord::Migration
  def change
    rename_column :projects, :estimated_completion_quarter, :estimated_start_quarter
    rename_column :projects, :estimated_completion_year, :estimated_start_year
  end
end
