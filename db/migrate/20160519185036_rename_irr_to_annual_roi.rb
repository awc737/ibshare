class RenameIrrToAnnualRoi < ActiveRecord::Migration
  def change
    rename_column :projects, :irr, :annual_roi
  end
end
