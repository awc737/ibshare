class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.decimal :funded_so_far
      t.string :legal_entity_name
      t.string :location
      t.string :title
      t.decimal :number_of_acres
      t.integer :number_of_buildings
      t.integer :number_of_units
      t.decimal :projected_value
      t.string :term
      t.decimal :total_debt_required
      t.decimal :total_investment_required
      t.string :type_of_development
      t.decimal :minimum_investment
      t.decimal :projected_gross_profit
      t.integer :estimated_completion_quarter
      t.integer :estimated_completion_year
      t.decimal :projected_land_costs
      t.decimal :projected_hard_costs
      t.decimal :projected_soft_costs
      t.text :project_pitch

      t.timestamps null: false
    end
  end
end
