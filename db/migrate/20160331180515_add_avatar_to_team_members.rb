class AddAvatarToTeamMembers < ActiveRecord::Migration
  def self.up
    add_attachment :team_members, :avatar
  end

  def self.down
    remove_attachment :team_members, :avatar
  end
end
