class ChangeInvestmentTermType < ActiveRecord::Migration
  def change
    remove_column :projects, :investment_term
    add_column :projects, :investment_term, :integer
  end
end
