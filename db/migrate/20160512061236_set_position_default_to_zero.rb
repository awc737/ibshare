class SetPositionDefaultToZero < ActiveRecord::Migration
  def change
    change_column :uploads, :position, :integer, default: 0
  end
end
