class AddPreLaunchFieldsToProject < ActiveRecord::Migration
  def change
    add_column :projects, :irr, :decimal
    add_column :projects, :property_details, :text
    add_column :projects, :financial_summary, :text
    add_column :projects, :investment_offering, :text
    add_column :projects, :market_summary, :text
  end
end
