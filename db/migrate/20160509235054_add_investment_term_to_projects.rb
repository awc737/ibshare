class AddInvestmentTermToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :investment_term, :string
  end
end
