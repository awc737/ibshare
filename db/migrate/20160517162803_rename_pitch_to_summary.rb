class RenamePitchToSummary < ActiveRecord::Migration
  def change
    rename_column :projects, :pitch, :summary
  end
end
