class AddFeaturedToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :featured, :boolean, default: false
  end
end
