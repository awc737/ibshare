# frozen_string_literal: true
class Upload < ActiveRecord::Base
  belongs_to :project
  has_attached_file :attachment
  do_not_validate_attachment_file_type :attachment
end
