# frozen_string_literal: true
class Document < Upload
  belongs_to :project
end
