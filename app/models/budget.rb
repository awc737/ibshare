# frozen_string_literal: true
class Budget < Upload
  belongs_to :project
end
