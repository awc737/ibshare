# frozen_string_literal: true
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validate :password_complexity

  def password_complexity
    return unless password.present?

    unless password =~ /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)./
      errors.add(
        :password,
        'requires at least one lowercase letter, ' \
        'one uppercase letter, and one digit'
      )
    end
  end
end
