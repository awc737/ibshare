# frozen_string_literal: true
class Project < ActiveRecord::Base
  has_many :budgets
  has_many :uploads
  has_many :documents
  has_many :media, -> { order position: :asc }
  has_many :team_members
end
