# frozen_string_literal: true
class Media < Upload
  belongs_to :project
  acts_as_list
end
