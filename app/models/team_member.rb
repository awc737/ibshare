# frozen_string_literal: true
class TeamMember < ActiveRecord::Base
  belongs_to :project
  has_attached_file :avatar
  do_not_validate_attachment_file_type :avatar
end
