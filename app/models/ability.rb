# frozen_string_literal: true
class Ability
  include CanCan::Ability

  def initialize(user)
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
    return unless user&.admin?

    can :access, :rails_admin
    can :manage, :all
  end
end
