# frozen_string_literal: true
class Offer < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
end
