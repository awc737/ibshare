# frozen_string_literal: true
module ClientHelper
  DEV_SERVER = 'http://localhost:8080/'
  PUBLIC_FOLDER = '/build/'

  def client_src
    javascript_include_tag env_specific_url('client.js')
  end

  def app_styles
    stylesheet_link_tag PUBLIC_FOLDER + 'client.css' unless using_node_server?
  end

  def vendor_styles
    stylesheet_link_tag env_specific_url('vendor.css')
  end

  def env_specific_url(resource)
    base = using_node_server? ? DEV_SERVER : PUBLIC_FOLDER
    base + resource
  end

  def using_node_server?
    return false if ENV['CI']
    Rails.env.development? || Rails.env.test?
  end
end
