# frozen_string_literal: true
class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@infobrij.com'
  layout 'mailer'
end
