# frozen_string_literal: true
class Project < ActiveRecord::Base
  class Create < Trailblazer::Operation
    attr_reader :params

    include Dispatch

    contract do
      feature Disposable::Twin::Persisted

      property :annual_roi
      property :estimated_start_quarter
      property :estimated_start_year
      property :financial_summary
      property :investment_offering
      property :investment_term
      property :legal_entity_name
      property :legal_entity_name
      property :location
      property :market_summary
      property :minimum_investment
      property :number_of_acres
      property :number_of_buildings
      property :number_of_units
      property :projected_gross_profit
      property :projected_hard_costs
      property :projected_land_costs
      property :projected_soft_costs
      property :property_details
      property :summary
      property :title
      property :total_debt_required
      property :total_investment_required
      property :type_of_development

      collection :budgets, populate_if_empty: Budget do
        property :name, virtual: true
        property :title

        validates :name, presence: true
        validates :title, presence: true
      end

      collection :documents, populate_if_empty: Document do
        property :name, virtual: true
        property :title

        validates :name, presence: true
        validates :title, presence: true
      end

      collection :media, populate_if_empty: Media do
        property :name, virtual: true
        property :title
        property :featured

        validates :name, presence: true
        validates :title, presence: true
      end

      collection :team_members, populate_if_empty: TeamMember do
        property :avatar, virtual: true
        property :name
        property :title
        property :bio

        validates :name, presence: true
        validates :title, presence: true
      end
    end

    callback :after_save do
      collection :budgets do
        on_add :process_upload!
      end

      collection :documents do
        on_add :process_upload!
      end

      collection :media do
        on_add :process_upload!
      end

      collection :team_members do
        on_add :process_avatar!
      end
    end

    def process(params)
      @model = Project.new
      @params = params

      validate(project_params) do |form|
        form.save
        dispatch! :after_save
      end
    end

    def process_avatar!(twin, _options)
      ProcessAttachmentJob.perform_later twin.model, 'avatar', twin.avatar
    end

    def process_upload!(twin, _options)
      ProcessAttachmentJob.perform_later twin.model, 'attachment', twin.name
    end

    private

    def project_params
      params[:project].reject(&method(:empty_value?))
    end

    def empty_value?(_key, value)
      value.nil? || value == [{}]
    end
  end
end
