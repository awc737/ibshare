# frozen_string_literal: true
class Project < ActiveRecord::Base
  class Index < Trailblazer::Operation
    include Collection

    def model!(_params)
      ProjectRepresenter.for_collection.new(
        Project.includes(:documents, :media, :team_members)
      )
    end
  end
end
