# frozen_string_literal: true
class Project < ActiveRecord::Base
  class ProcessAttachmentJob < ActiveJob::Base
    attr_reader :file_name

    queue_as :default

    def perform(record, field, file_name)
      @file_name = file_name
      record.update field => remote_file.public_url
      remote_file.destroy
    end

    private

    def remote_directory
      Storage.directories.find do |directory|
        directory.key == Rails.application.secrets.storage_bucket
      end
    end

    def remote_file
      path = Rails.application.secrets.storage_directory + '/' + file_name
      remote_directory.files.get path
    end
  end
end
