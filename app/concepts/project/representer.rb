# frozen_string_literal: true
require 'representable/json'

class ProjectRepresenter < Representable::Decorator
  include Representable::JSON

  property :annual_roi
  property :estimated_start_quarter
  property :estimated_start_year
  property :financial_summary
  property :id
  property :investment_offering
  property :investment_term
  property :legal_entity_name
  property :location
  property :market_summary
  property :minimum_investment
  property :number_of_acres
  property :number_of_buildings
  property :number_of_units
  property :projected_gross_profit
  property :projected_hard_costs
  property :projected_land_costs
  property :projected_soft_costs
  property :property_details
  property :summary
  property :title
  property :total_debt_required
  property :total_investment_required
  property :type_of_development

  collection :media, class: Media do
    get_public_url = -> (represented:, **) { represented.attachment.public_url }

    property :id
    property :title
    property :featured
    property :public_url, getter: get_public_url
  end

  collection :team_members, class: TeamMember do
    get_public_url = -> (represented:, **) { represented.avatar.public_url }

    property :id
    property :name
    property :title
    property :bio
    property :public_url, getter: get_public_url
  end
end
