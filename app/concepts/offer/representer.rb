# frozen_string_literal: true
require 'representable/json'

class OfferRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :amount
  property :project_id
  property :user_id
end
