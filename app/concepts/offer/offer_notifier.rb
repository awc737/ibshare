# frozen_string_literal: true
class Offer < ActiveRecord::Base
  class OfferNotifier < ApplicationMailer
    prepend_view_path Rails.root.join('app/concepts/offer/views')

    def notify_offer_manager(offer)
      @offer = offer
      @user = offer.user

      mail(
        to: offers_email,
        subject: "[#{offer.project_id}] New Offer: #{offer.amount}"
      )
    end

    private

    def offers_email
      ENV['INFOBRIJ_OFFERS_EMAIL'] || 'support@infobrij.com'
    end
  end
end
