# frozen_string_literal: true
class Offer < ActiveRecord::Base
  class Create < Trailblazer::Operation
    contract do
      property :amount, validates: { presence: true }
      property :project_id, validates: { presence: true }
      property :user_id, validates: { presence: true }
    end

    def process(params)
      @model = Offer.new

      validate params[:offer] do |f|
        f.save
        notify_offer_manager!
      end
    end

    private

    def notify_offer_manager!
      OfferNotifier
        .notify_offer_manager(@model)
        .deliver_later
    end
  end
end
