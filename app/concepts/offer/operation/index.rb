# frozen_string_literal: true
class Offer < ActiveRecord::Base
  class Index < Trailblazer::Operation
    include Collection

    def model!(_params)
      OfferRepresenter.for_collection.new Offer.all
    end
  end
end
