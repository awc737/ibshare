# frozen_string_literal: true
require 'app_security'

class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to main_app.root_path, alert: exception.message
  end

  AppSecurity.protect self do |conf|
    conf.ssl = ENV['FORCE_SSL'] || Rails.env.production?
    conf.http_basic_auth = ENV['PREFLIGHT']
  end

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :sign_up, keys: [:first_name, :last_name]
  end
end
