# frozen_string_literal: true
module Api
  class ProjectsController < ApiBaseController
    def index
      render json: collection(Project::Index).model
    end

    def create
      run Project::Create do |operation|
        return render json: { project: operation.model }, status: :created
      end

      head :unprocessable_entity, content_type: 'text/html'
    end
  end
end
