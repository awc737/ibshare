# frozen_string_literal: true
module Api
  class UploadsController < ApiBaseController
    def sign
      render json: {
        fileName: unique_filename,
        signedUrl: signed_url,
        originalName: params[:objectName]
      }
    end

    private

    def signed_url
      Storage.put_object_url(
        bucket_name,
        object_name,
        2.minutes.from_now.to_i,
        response_headers
      )
    end

    def response_headers
      { 'Content-Type' => params[:contentType],
        'x-amz-acl' => 'public-read' }
    end

    def bucket_name
      Rails.application.secrets.storage_bucket
    end

    def object_name
      Rails.application.secrets.storage_directory + '/' + unique_filename
    end

    def unique_filename
      @unique_filename ||= SecureRandom.uuid + File.extname(params[:objectName])
    end
  end
end
