# frozen_string_literal: true
module Api
  class OffersController < ApplicationController
    def index
      render json: collection(Offer::Index).model
    end

    def create
      Offer::Create.run(offer_params) do |operation|
        return render json: { offer: operation.model }, status: :created
      end

      head :unprocessable_entity, content_type: 'text/html'
    end

    private

    def offer_params
      params[:offer][:user_id] = current_user.id
      params
    end
  end
end
