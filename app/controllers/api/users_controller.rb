# frozen_string_literal: true
module Api
  class UsersController < ApiBaseController
    def current
      render json: current_user
    end
  end
end
