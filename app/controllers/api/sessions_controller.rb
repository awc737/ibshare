# frozen_string_literal: true
module Api
  class SessionsController < Devise::SessionsController
    clear_respond_to
    respond_to :json

    def create
      unless valid_credentials?
        error = { error: 'Invalid email or password' }
        return render json: error, status: 401
      end

      super
    end

    private

    def valid_credentials?
      user&.valid_password? params[:user][:password]
    end

    def user
      @user ||= User.find_for_authentication email: params[:user][:email]
    end
  end
end
