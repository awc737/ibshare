# frozen_string_literal: true
module Api
  class ApiBaseController < ApplicationController
    before_action :authenticate_user!

    def authenticate_user!
      unless user_signed_in?
        if controller_name == 'users'
          return render json: { body: 'authentication pending' }
        end

        render json: { body: 'authentication required' }, status: :unauthorized
      end
    end
  end
end
