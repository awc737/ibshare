# frozen_string_literal: true
module Api
  class PasswordsController < Devise::PasswordsController
    clear_respond_to
    respond_to :json
  end
end
