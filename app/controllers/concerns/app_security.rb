# frozen_string_literal: true
class AppSecurity
  def self.protect(*args, &config)
    new(*args, &config).enforce!
  end

  attr_accessor :ssl
  attr_accessor :http_basic_auth

  def initialize(controller)
    @controller = controller
    yield self if block_given?
  end

  def enforce!
    controller.force_ssl if force_ssl?
    force_http_basic_auth if http_basic_auth?
  end

  private

  attr_reader :controller

  def force_ssl?
    ssl.present?
  end

  def http_basic_auth?
    http_basic_auth.present?
  end

  def force_http_basic_auth
    controller.http_basic_authenticate_with(
      name: 'infobrij',
      password: 'Platform8*8'
    )
  end
end
