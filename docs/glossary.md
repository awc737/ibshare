# Project Glossary

Here are some of the terms you might encounter as you work on this project and what they mean.

* **Appraisal Management Company (AMC):** A company that manages the appraisal process independently of any parties involved in a transaction.  The AMC will usually engage the services of an approved appraiser to complete the appraisal.

* **Assessor's Parcel Number (APN):** The legal description of a piece of property.

* **Deal:** An individual transaction that includes the **Terms** of the transaction.  An **Investor** making an investment in a **Project** under agreed-upon **Terms** is a **Deal**.  It generally takes many **Deal**s in order to fully fund a **Project**.  Sometimes, **Deal** and **Project are used interchangeably, but for the purposes of this software, we will distinguish these two terms.

* **Deal Exit:**  A **Deal** is considered to be complete when the **Project** has been completed and the **Investors** and **Lenders** paid, or when it has gone into **Default**.

* **Deal Package:** A packet of information prepared by a **Sponsor** that informs potential **Investor**s about the details of a **Project**.  Includes a **Private Placement Memorandum**, a **Subscription Agreement**, and a **Pro Forma**, along with additional information about the **Project** and the **Sponsor**.
 
* **Default:** A **Project** is considered to be in **Default** when its maturity date is elapsed.  It can also be considered to be in **Default** if it is significantly over budget and/or schedule.  5 - 10% overruns are not considered significant.  30% is definitely significant.  Anything in between is a gray area.

* **Investor:** A person or entity that wants to invest money into one or more **Project**s.

* **Lender:** A person or entity that wants to loan money to one or more **Sponsor**s to enable them to complete one or more **Project**s.

* **Master Appraisal Institute (MAI):** A designation that is "held by appraisers who are experienced in the valuation and evaluation of commercial, industrial, residential, and other types of properties, and advise clients on real estate investment decisions." (http://www.appraisalinstitute.org/designations/)  This designation is generally required to be held be appraisers that are approved by lenders.

* **Preliminary Title Report (PTR):** A report that outlines the current status of the title of a piece of land.  It contains information about the current owners of the property, any boundary or survey issues, and any liens and encumbrances on the property.

* **Private Placement Memorandum (PPM):** Part of a **Deal Package**.
 
* **Pro Forma:** Part of a **Deal Package**.

* **Project:** Something that a **Sponsor** wants to build.  In real estate, this might be a shopping mall or apartment complex.  The lifespan of a **Project** starts with a piece of raw land that the **Sponsor** owns or otherwise has contractually tied up and ends with an income-producing building.

* **Service:** A support task required to complete a **Deal**.  **Service** includes appraisals, evaluating **Sponsor**s, **Investor**s and **Project**s, escrow services, title services, etc.

* **Service Provider:** A person or entity who provides one or more **Service**s in order to enable a **Project** to be completed.

* **Sponsor:**  A person or entity that wants to build a **Project**.  They are seeking **Investor**s and **Lender**s to fund the **Project**.
 
* **Subscription Agreement:** Part of a **Deal Package**.

* **Terms:** The details of a particular **Deal**.  Might include the amount of money involved, the expected rate of return, etc.

* **Title Policy:** An policy that insures that a title holder has clear title on a property.  If later information is uncovered that clouds the title, the title insurance underwriter assumes the costs of resolving the issues and clearing the title.
