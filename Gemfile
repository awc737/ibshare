# frozen_string_literal: true
source 'https://rubygems.org'
ruby '2.3.0'

gem 'rails', '~> 4.2'

gem 'devise'
gem 'factory_girl_rails', '~> 4.0' # Move at launch
gem 'faker' # Move at launch
gem 'fog'
gem 'jbuilder', '~> 2.0'
gem 'paperclip', '~> 4.3'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'trailblazer-rails'
gem 'trailblazer'
gem 'uglifier', '>= 1.3.0'
gem 'sidekiq', '~> 4.1.2'
gem 'sinatra', require: nil
gem 'acts_as_list'
gem 'rails_admin'
gem 'cancancan'

group :production do
  gem 'rails_12factor'
end

group :development, :test do
  gem 'capybara', '~> 2.5'
  gem 'database_cleaner'
  gem 'dotenv-rails', '~> 2.1'
  gem 'launchy'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.4'
  gem 'selenium-webdriver'
end

group :development do
  gem 'guard-rspec', require: false
  gem 'mailcatcher', require: false
  gem 'rubocop', '~> 0.37.0'
  gem 'spring'
  gem 'web-console', '~> 3.0'
end
