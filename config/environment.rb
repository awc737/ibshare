# frozen_string_literal: true
# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.smtp_settings = {
  address: Rails.application.secrets.smtp_address,
  port: Rails.application.secrets.smtp_port,
  authentication: :plain,
  user_name: Rails.application.secrets.sendgrid_username,
  password: Rails.application.secrets.sendgrid_password,
  domain: 'app.infobrij.com',
  enable_starttls_auto: true
}
