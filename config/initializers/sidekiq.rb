# frozen_string_literal: true
require 'sidekiq'
Sidekiq.configure_client do |config|
  config.redis = { size: 1, host: ENV['REDIS_HOST'] }
end

Sidekiq.configure_server do |config|
  config.redis = { size: 9, host: ENV['REDIS_HOST'] }
end
