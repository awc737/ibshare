# frozen_string_literal: true
require 'spec_helper'
require 'active_support/core_ext/object/blank'
require_relative '../../app/controllers/concerns/app_security'

describe AppSecurity do
  subject { described_class.new controller }

  let(:controller) do
    double(
      :controller,
      force_ssl: -> {},
      http_basic_authenticate_with: ->(params) {}
    )
  end

  describe 'when protection is disabled' do
    before { subject.enforce! }

    specify do
      expect(controller)
        .not_to have_received(:force_ssl)
    end

    specify do
      expect(controller)
        .not_to have_received(:http_basic_authenticate_with)
    end
  end

  describe 'SSL protection' do
    before do
      subject.ssl = true
      subject.enforce!
    end

    it { expect(controller).to have_received(:force_ssl) }
  end

  describe 'HTTP Basic Auth protection' do
    before do
      subject.http_basic_auth = true
      subject.enforce!
    end

    it { expect(controller).to have_received(:http_basic_authenticate_with) }
  end

  describe '.protect' do
    let(:app_security) do
      instance_double 'AppSecurity', enforce!: -> {}
    end

    before do
      allow(described_class)
        .to receive(:new)
        .and_return app_security
    end

    it 'initializes a new object and autoruns #enforce!' do
      described_class.protect(controller)
      expect(app_security).to have_received(:enforce!)
    end
  end
end
