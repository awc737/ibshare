# frozen_string_literal: true
require 'rails_helper'

describe Offer::Create do
  describe 'with required attributes' do
    let!(:amount) { '10000.00' }
    let!(:project) { create :project }
    let!(:user) { create :user }

    let(:params) do
      {
        offer: {
          amount: amount,
          user_id: user.id,
          project_id: project.id
        }
      }
    end

    it 'persists' do
      expect { Offer::Create.(params) }.to change { Offer.count }.by 1
      offer = Offer.last

      expect(offer.amount).to eq amount.to_d
      expect(offer.project_id).to eq project.id
      expect(offer.user_id).to eq user.id
    end

    describe 'notifying an offer manager' do
      Sidekiq::Testing.fake!

      it 'delivers later' do
        expect { Offer::Create.(params) }
          .to change { Sidekiq::Worker.jobs.size }
          .by 1
      end
    end
  end
end
