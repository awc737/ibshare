# frozen_string_literal: true
require 'rails_helper'

describe ProjectRepresenter do
  let(:project) { build_stubbed :project, title: 'Title' }

  it 'can convert to a hash' do
    result = described_class.new(project).to_hash

    expect(result).to include 'title' => 'Title'
  end

  context 'with related media' do
    let(:project) { create :project_with_media }

    it 'includes media title and attachment info' do
      media = described_class.new(project).to_hash['media']

      expect(media).to be_a Array
      expect(media.first['title']).to be_a String
      expect(media.first['public_url']).to be_a String
    end
  end
end
