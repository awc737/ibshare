# frozen_string_literal: true
require 'rails_helper'

describe Project::Index do
  let(:params) { {} }
  let!(:project) { create_list :project_with_media, 2, media_count: 2 }

  it 'includes media' do
    operation = described_class.present params
    result = operation.model.to_hash

    expect(result.size).to eq 2
    expect(result.first['media'].size).to eq 2
    expect(result.first['media'].first['title']).to be_a String
  end
end
