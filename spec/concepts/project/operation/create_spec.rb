# frozen_string_literal: true
require 'rails_helper'

describe Project::Create do
  describe 'with minimal attributes' do
    let(:params) { { project: { title: 'Test Project' } } }

    it 'persists' do
      expect { Project::Create.(params) }.to change { Project.count }.by 1
      expect(Project.last.title).to eq 'Test Project'
    end
  end
end
