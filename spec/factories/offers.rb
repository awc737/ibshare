# frozen_string_literal: true
FactoryGirl.define do
  factory :offer do
    amount '10000.00'
    user nil
    project nil
  end
end
