# frozen_string_literal: true
FactoryGirl.define do
  factory :project do
    estimated_start_quarter { rand 1..4 }
    estimated_start_year { rand 2016..2020 }
    financial_summary { Faker::Lorem.paragraph 10 }
    investment_offering { Faker::Lorem.paragraph 10 }
    annual_roi { rand 8..20 }
    legal_entity_name { Faker::Company.name }
    location { "#{Faker::Address.city}, #{Faker::Address.state_abbr}" }
    market_summary { Faker::Lorem.paragraph 10 }
    minimum_investment { rand 5_000...50_000 }
    number_of_acres { rand 1..100 }
    number_of_buildings { rand 1..100 }
    number_of_units { rand 1..100 }
    projected_gross_profit { rand 100_000..10_000_000 }
    projected_hard_costs { rand 100_000..10_000_000 }
    projected_land_costs { rand 100_000..10_000_000 }
    projected_soft_costs { rand 100_000..10_000_000 }
    property_details { Faker::Lorem.paragraph 10 }
    summary { Faker::Lorem.paragraph 10 }
    title { Faker::Company.name }
    total_debt_required { rand 100_000..10_000_000 }
    total_investment_required { rand 100_000..10_000_000 }
    type_of_development do
      ['Single Family – For Sale',
       'Multi-Family – For Sale',
       'Apartments – For Rent',
       'Apartments – For Sale',
       'Hotel / Motel',
       'Mixed Use – Live/Work, Multi w/ Commercial',
       'Senior Housing – Independent, Assisted & Continuing Care',
       'Student Housing - Dormitory',
       'Manufactured Housing',
       'Health Care'
      ].sample
    end

    factory :project_with_media do
      transient do
        media_count { rand 1..5 }
      end

      after :create do |project, evaluator|
        create_list :media, evaluator.media_count, project: project
      end
    end

    factory :project_fully_loaded do
      transient do
        media_count { rand 1..5 }
        team_member_count { rand 1..6 }
      end

      after :create do |project, evaluator|
        create_list :media, evaluator.media_count, project: project
        create_list :team_member, evaluator.team_member_count, project: project
      end
    end
  end
end
