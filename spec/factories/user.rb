# frozen_string_literal: true
FactoryGirl.define do
  factory :user do
    first_name 'John'
    last_name 'Smith'
    password 'Password1'
    sequence :email do |n|
      n == 1 ? 'user@infobrij.com' : "user#{n}@infobrij.com"
    end
  end
end
