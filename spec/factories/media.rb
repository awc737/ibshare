# frozen_string_literal: true
FactoryGirl.define do
  factory :media do
    title { Faker::Lorem.words(rand(1..5)).join(' ') }
    attachment do
      filename = "#{rand(1..4)}_#{rand(1..4)}.jpg"
      File.new "spec/files/project_media/#{filename}"
    end
  end
end
