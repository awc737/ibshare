# frozen_string_literal: true
FactoryGirl.define do
  factory :team_member do
    name { Faker::Name.name }
    title do
      ['CEO',
       'CTO',
       'VP of Marketing',
       'Administrator of Awesome',
       'Chief Magnus',
       'Janitor',
       'Receptionist',
       'Engineer',
       'Torch Bearer'
      ].sample
    end
    bio { Faker::Lorem.paragraph rand 1..5 }
    avatar do
      filename = "#{rand(1..12)}.jpg"
      File.new "spec/files/team_members/#{filename}"
    end
  end
end
