# frozen_string_literal: true
require 'rails_helper'

feature 'Creating a Project', js: true do
  let!(:user) { build :user }
  before { user.save }

  scenario 'should persist the project and nested resources' do
    create_project = proc do
      visit 'user/sign_in'
      fill_in 'email', with: user.email
      fill_in 'password', with: user.password
      click_on 'Continue'

      sleep 1 # temporary until there is a link to create new projects
      visit 'projects/new'

      fill_in 'title', with: 'Project Title'
      fill_in 'legalEntityName', with: 'Legal Name'
      fill_in 'location', with: 'Somewhere'
      click_next

      select 'Single Family – For Sale', from: 'typeOfDevelopment'
      fill_in 'numberOfUnits', with: '10'
      fill_in 'numberOfAcres', with: '50'
      fill_in 'numberOfBuildings', with: '5'
      click_next

      fill_in 'totalInvestmentRequired', with: '10000'
      fill_in 'totalDebtRequired', with: '50000'
      fill_in 'minimumInvestment', with: '100000'
      select '6 months', from: 'investmentTerm'
      fill_in 'annualRoi', with: '10'
      click_next

      fill_in 'projectedGrossProfit', with: '10000'
      select 'Q3', from: 'estimatedStartQuarter'
      select '2018', from: 'estimatedStartYear'
      click_next

      fill_in 'projectedLandCosts', with: '10000'
      fill_in 'projectedHardCosts', with: '50000'
      fill_in 'projectedSoftCosts', with: '100000'
      click_next

      fill_in 'summary', with: 'Project Summary...'
      click_next

      fill_in 'propertyDetails', with: 'Property Details...'
      click_next

      fill_in 'financialSummary', with: 'Financial Summary...'
      click_next

      fill_in 'investmentOffering', with: 'Investment Offering...'
      click_next

      fill_in 'marketSummary', with: 'Market Smmary...'
      click_next

      click_next

      click_add_button
      fill_in 'teamMembers[0].name', with: 'John'
      fill_in 'teamMembers[0].title', with: 'CEO'
      fill_in 'teamMembers[0].bio', with: 'Bio'
      click_add_button
      fill_in 'teamMembers[0].name', with: 'Mark'
      fill_in 'teamMembers[0].title', with: 'CFO'
      fill_in 'teamMembers[0].bio', with: 'Bio'
      click_next

      click_next

      expect(page).to have_content 'Project Title'.upcase
    end

    expect(&create_project).to change { Project.count }.by 1
    expect(Project.last.team_members.map(&:name)).to include 'John', 'Mark'
  end

  def click_next
    all('button', text: 'chevron_right').first.click
  end

  def click_add_button
    success_measure = proc { all('input').count }

    wait_for_increment_of success_measure do
      find('.FieldManager button').click
    end
  end

  def wait_for_increment_of(success_measure)
    initial_count = success_measure.call

    Timeout.timeout Capybara.default_max_wait_time do
      yield
      loop until success_measure.call > initial_count
    end
  end
end
