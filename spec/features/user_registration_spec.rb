# frozen_string_literal: true
require 'rails_helper'

feature 'Registration', js: true do
  let(:attributes) { build_stubbed :user }

  scenario 'should persist a new user' do
    register_user = proc do
      visit root_path
      click_on 'Sign Up'

      fill_in 'firstName', with: attributes.first_name
      fill_in 'lastName', with: attributes.last_name
      fill_in 'email', with: attributes.email
      fill_in 'password', with: attributes.password
      fill_in 'passwordConfirmation', with: attributes.password

      find('span', text: 'Accept our').click

      click_on 'GET STARTED'

      expect(page).to have_content 'Welcome'
      sleep 1
    end

    expect(&register_user).to change { User.count }.by 1

    user = User.last
    expect(user.first_name).to eq attributes.first_name
    expect(user.last_name).to eq attributes.last_name
    expect(user.email).to eq attributes.email
  end
end
