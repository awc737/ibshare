import React from 'react'
import { Router } from 'react-router'
import { FixNamedRoutesSupport as allowNames } from 'react-router-named-routes'

import { AppRoutes } from 'modules/app'

allowNames(AppRoutes)

export default function AppRouter({ history }) {
  return (
    <Router children={AppRoutes} history={history} />
  )
}
