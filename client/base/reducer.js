import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { routerReducer } from 'react-router-redux'

import { AppReducer } from 'modules/app'

export default combineReducers({
  app: AppReducer,
  form: formReducer,
  routing: routerReducer
})
