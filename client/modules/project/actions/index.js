import ActionTypes from '../constants/ActionTypes'
import { toServer } from '../form'
import { callApi, navigateTo } from 'utils'

export function addProject(data) {
  const onSuccess = ({ payload: { project: { id } } }) =>
    navigateTo('project', { id })

  return callApi({
    endpoint: '/api/projects',
    method: 'POST',
    body: { project: toServer(data) },
    types: [
      ActionTypes.CREATE_PROJECT_REQUEST,
      ActionTypes.CREATE_PROJECT_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  }, { onSuccess })
}

export function fetchProjects() {
  return callApi({
    endpoint: '/api/projects',
    types: [
      ActionTypes.FETCH_PROJECTS_REQUEST,
      ActionTypes.FETCH_PROJECTS_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  })
}
