import React, { Component } from 'react'

import { Content, Frame } from 'modules/layout'
import { Navigation } from 'modules/app'
import { WelcomeMessage } from 'modules/user'

export default class Layout extends Component {
  componentWillMount() {
    const { fetchUser, fetchProjects, fetchOffers } = this.props

    fetchUser()
    fetchProjects()
    fetchOffers()

    this.offersInterval = setInterval(fetchOffers, 5000)
  }

  componentWillUnmount() {
    clearInterval(this.offersInterval)
  }

  render() {
    const { children } = this.props

    return (
      <Frame className='vertical'>
        <Content className='shrink collapse'>
          <Navigation />
        </Content>

        {children}

        <WelcomeMessage />
      </Frame>
    )
  }
}
