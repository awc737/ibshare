import React from 'react'
import classnames from 'classnames'
import { compose, defaultTo, divide, multiply } from 'ramda'

import { ProgressBar } from 'modules/ui'
import { formatCurrency } from 'utils'

import styles from './styles.scss'

const format = compose(formatCurrency, parseInt)
const percent = compose(defaultTo(0), parseInt, multiply(100), divide)

export default function FundingProgress(props) {
  const { hasProgressBar, large, raised = 0, required = 0 } = props
  const percentComplete = percent(raised, required)

  const classNames = classnames(
    styles.root, { [styles.large]: large }
  )

  return (
    <div className={classNames}>
      {hasProgressBar && <ProgressBar complete={percentComplete} />}

      <span className={styles.raised}>
        {format(raised)}
      </span>

      <span>
        &nbsp;of {format(required)}
        {!hasProgressBar &&
          <span className={styles.percent}>
            {percentComplete}%
          </span>}
        &nbsp;raised
      </span>
    </div>
  )
}
