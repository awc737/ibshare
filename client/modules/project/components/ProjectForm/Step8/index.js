import React from 'react'

import { Content, Block } from 'modules/layout'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step8({ financialSummary }) {
  const stepProps = {
    heading: "Now let's fill out your project description",
    title: 'Your Project',
    titleicon: 'location_city',
    page: 8
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content>
            <label className={styles.label}>
              Financial Summary
            </label>

            <textarea
                {...financialSummary}
                className={styles.textarea} />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}
