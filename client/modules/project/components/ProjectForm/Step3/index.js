import React from 'react'
import classnames from 'classnames'
import { unfold } from 'ramda'

import { Content, Block } from 'modules/layout'
import { CurrencyInput, PercentInput } from 'modules/ui'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step3(props) {
  const {
    annualRoi,
    totalInvestmentRequired,
    totalDebtRequired,
    minimumInvestment
  } = props

  const stepProps = {
    heading: "Next let's fill out your project's investment numbers",
    title: 'Investment Numbers',
    titleicon: 'trending_up',
    page: 3
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content className='small-12 large-6'>
            <CurrencyInput
                {...totalInvestmentRequired}
                hint='ex. $3,000,000'
                label='Total Investment:&nbsp; Equity to be Raised' />
          </Content>

          <Content className='small-12 large-6'>
            <CurrencyInput
                {...totalDebtRequired}
                hint='ex. $ 12,000,000'
                label='Total Debt Requirement' />
          </Content>

          <Content className='small-12 medium-12 large-6'>
            <CurrencyInput
                {...minimumInvestment}
                hint='ex. $50,000'
                label='Minimum Investment' />
          </Content>

          <Content className='small-12 medium-12 large-3'>
            <label className={styles.label}>
              Investment Term
            </label>

            <InvestmentTermSelect {...props} />
          </Content>

          <Content className='small-12 medium-12 large-3'>
            <PercentInput
                {...annualRoi}
                hint='ex. 12%'
                label='Annual ROI' />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}

function InvestmentTermSelect({ investmentTerm }) {
  const valueIsSelected = !investmentTerm.value
  const classNames = classnames(styles.select, {
    [styles.disabled]: valueIsSelected
  })

  const termRange = unfold(n => n > 72 ? false : [n, n + 3], 3)

  return (
    <select {...investmentTerm} className={classNames}>
      {termRange.map((value, index) =>
        <option key={index} value={value}>{value} months</option>
      )}
    </select>
  )
}
