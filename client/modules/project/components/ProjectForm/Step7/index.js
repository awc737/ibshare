import React from 'react'

import { Content, Block } from 'modules/layout'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step7({ propertyDetails }) {
  const stepProps = {
    heading: "Now let's fill out your project description",
    title: 'Your Project',
    titleicon: 'location_city',
    page: 7
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content>
            <label className={styles.label}>
              Property Details
            </label>

            <textarea
                {...propertyDetails}
                className={styles.textarea} />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}
