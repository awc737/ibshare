import React from 'react'
import { either, isEmpty, last, partial } from 'ramda'

import { Content, Block } from 'modules/layout'
import {
  DocumentUploader,
  FieldManager,
  Input,
  RadioButton,
  RadioGroup
} from 'modules/ui'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step13(props) {
  const {
    addValue,
    handleSubmit,
    media,
    featuredImage
  } = props

  const stepProps = {
    heading: "Let's start with your project's basic information",
    title: 'Basic Information',
    titleicon: 'insert_drive_file',
    page: 13
  }

  const addMedia = partial(addValue, ['media'])

  return (
    <Step {...stepProps} handleSubmit={handleSubmit}>
      <Content className='small-12'>
        <h4 className={styles.subtitle}>Upload media files of your project</h4>
        <label className={styles.toplabel}>
          ex. Elevations Drawings, Floor Plans, Site Plan, Renderings,
          Aerial and Site Photos
        </label>
        <br />
        <Block>
            {media.reverse().map(({ name, title, featured }, index) =>
              <Content
                  key={index}
                  className='small-12 medium-6 large-4 text-center right'>
                <DocumentUploader
                    {...name}
                    autoPrompt
                    icon='photo'
                    setTitle={title.onChange} />
                <Input {...title} label='Title' hint='ex. Floor Plan' />
                <RadioGroup {...featuredImage}>
                  <RadioButton
                      {...featured}
                      label='Featured Image'
                      value={index} />
                </RadioGroup>
              </Content>
            )}
            {shouldShowFieldManager(media) &&
              <FieldManager
                  addValue={addMedia}
                  fieldLabel='Upload Media' />}
        </Block>
      </Content>
    </Step>
  )
}

function shouldShowFieldManager(media) {
  return either(lastMediaHasValue, isEmpty)(media)
}

function lastMediaHasValue(media) {
  return Boolean(last(media) && last(media).name.value)
}
