import React from 'react'

import { Content, Block } from 'modules/layout'
import { TextArea } from 'modules/ui'
import Step from '../Step'

export default function Step6({ summary }) {
  const stepProps = {
    heading: "Now let's fill out your project description",
    title: 'Your Project',
    titleicon: 'location_city',
    page: 6
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content>
            <TextArea {...summary} label='Project Summary' />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}
