import React from 'react'
import { either, isEmpty, last, partial } from 'ramda'

import { Content, Block } from 'modules/layout'
import { DocumentUploader, FieldManager, Input } from 'modules/ui'
import Step from '../Step'

export default function Step11({ addValue, documents }) {
  const stepProps = {
    heading: "Now let's fill out your project description",
    title: 'Your Project',
    titleicon: 'location_city',
    page: 11
  }

  const addDocument = partial(addValue, ['documents'])

  return (
    <Step {...stepProps}>
      <Content className='small-12'>
        <Block>
            {documents.reverse().map(({ name, title }, index) =>
              <Content
                  key={index}
                  className='small-12 medium-6 large-4 text-center right'>
                <DocumentUploader
                    {...name}
                    autoPrompt
                    title={title.value}
                    setTitle={title.onChange} />
                <Input {...title} hint='ex. Entitlements, Appraisal, etc.' />
              </Content>
            )}

            {shouldShowFieldManager(documents) &&
              <FieldManager
                  addValue={addDocument}
                  fieldLabel='Upload Document' />}
        </Block>
      </Content>
    </Step>
  )
}

function shouldShowFieldManager(documents) {
  return either(lastDocumentHasValue, isEmpty)(documents)
}

function lastDocumentHasValue(documents) {
  return Boolean(last(documents) && last(documents).name.value)
}
