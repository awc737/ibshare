import { keyPressed, preventKeys } from 'utils'

export default function enforceDecimalFormat({ onChange, ...fieldAttrs }) {
  return {
    onChange: handleChange(onChange),
    onKeyPress: handleKeyPress,
    ...fieldAttrs
  }
}

function handleChange(callback) {
  return value => callback(fixAt2Decimals(value))
}

function handleKeyPress(event) {
  preventKeys('e', '-')(event)
  disallowMultipleDecimals(event)
}

function fixAt2Decimals(number) {
  const afterDecimal = number.split('.')[1]
  const lengthPast = afterDecimal && afterDecimal.length

  return lengthPast > 2 ? parseFloat(number).toFixed(2) : number
}

function disallowMultipleDecimals(event) {
  if (keyPressed('.', event) && !isDecimalAllowed(event.target.value)) {
    event.preventDefault()
  }
}

function isDecimalAllowed(value) {
  const isEmpty = value === ''
  const alreadyDecimal = value.indexOf('.') > -1

  return !isEmpty && !alreadyDecimal
}
