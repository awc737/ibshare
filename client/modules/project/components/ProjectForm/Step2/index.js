import React from 'react'

import { Content, Block } from 'modules/layout'
import { Input } from 'modules/ui'
import Step from '../Step'
import { developmentTypes } from '../../../models/Project'
import { preventKeys } from 'utils'
import enforceDecimalFormat from './enforceDecimalFormat'

import styles from '../styles.scss'

export default function Step2(props) {
  const { numberOfAcres, numberOfBuildings, numberOfUnits } = props

  const stepProps = {
    heading: "Let's start with your project's basic information",
    title: 'Basic Information',
    titleicon: 'insert_drive_file',
    page: 2
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content className='small-12'>
            <label className={styles.label}>Type</label>
            <DevelopmentTypeSelect {...props} />
          </Content>

          <Content className='small-12 medium-3'>
            <Input
                {...numberOfBuildings}
                label='Buildings'
                min={0}
                onKeyPress={preventKeys('e', '-', '.')}
                hint='5'
                type='number' />
          </Content>

          <Content className='small-12 medium-3'>
            <Input
                {...numberOfUnits}
                label='Units'
                min={0}
                onKeyPress={preventKeys('e', '-', '.')}
                hint='100'
                type='number' />
          </Content>

          <Content className='small-12 medium-3'>
            <Input
                {...enforceDecimalFormat(numberOfAcres)}
                label='Acres'
                min={0}
                hint='100'
                type='number' />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}

function DevelopmentTypeSelect({ typeOfDevelopment }) {
  return (
    <select {...typeOfDevelopment} className={styles.select}>
      <option value='' disabled></option>
      {developmentTypes.map((type, index) =>
        <option key={index} value={type}>{type}</option>
      )}
    </select>
  )
}
