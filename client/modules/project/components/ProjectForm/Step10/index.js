import React from 'react'

import { Content, Block } from 'modules/layout'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step10({ marketSummary }) {
  const stepProps = {
    heading: "Now let's fill out your project description",
    title: 'Your Project',
    titleicon: 'location_city',
    page: 10
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content>
            <label className={styles.label}>
              Market Summary
            </label>

            <textarea
                {...marketSummary}
                className={styles.textarea} />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}
