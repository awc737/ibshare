import React from 'react'
import { either, isEmpty, last, partial } from 'ramda'

import { Content, Block } from 'modules/layout'
import { AvatarUploader, FieldManager, Input } from 'modules/ui'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step12({ addValue, teamMembers }) {
  const stepProps = {
    heading: 'Adding your team',
    title: 'Your Team',
    titleicon: 'group',
    page: 12
  }

  const addTeamMember = partial(addValue, ['teamMembers'])

  return (
    <Step {...stepProps}>
      <Content className='small-12'>
        <Block>
            {teamMembers.reverse().map((teamMember, index) =>
              <TeamMemberFieldSet {...teamMember} key={index} />
            )}

            {shouldShowFieldManager(teamMembers) &&
              <FieldManager
                  collection={teamMembers}
                  addValue={addTeamMember}
                  fieldLabel={<FieldManagerLabel />} />
            }
        </Block>
      </Content>
    </Step>
  )
}

function TeamMemberFieldSet({ avatar, bio, name, title, previewUrl }) {
  return (
    <Content className='small-12 medium-6 large-4 text-center'>
      <AvatarUploader
          key={avatar.value}
          avatar={avatar}
          previewUrl={previewUrl} />

      <Input {...name} hint='Full Name' />

      <Input {...title} hint='Title' />

      <textarea
          {...bio}
          placeholder='Bio'
          className={styles.textarea} />
    </Content>
  )
}

function FieldManagerLabel() {
  return <span>Add Another<br />Team Member</span>
}

function shouldShowFieldManager(teamMembers) {
  return either(lastTeamMemberHasName, isEmpty)(teamMembers)
}

function lastTeamMemberHasName(teamMembers) {
  return Boolean(last(teamMembers) && last(teamMembers).name.value)
}
