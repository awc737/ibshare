import React from 'react'
import { Link } from 'react-router-named-routes'

import { Container, Content } from 'modules/layout'
import { Button, FontIcon } from 'modules/ui'

import styles from './styles.scss'

export default function Step({
  children,
  handleSubmit,
  heading,
  page,
  title,
  titleicon
}) {
  return (
    <Container className='small-12 medium-10 large-8'>
      <Content className={`${styles.header} medium-8`}>
        <h1>
          {heading}
        </h1>
      </Content>

      <Content className={styles.formCard}>
        <span className={`${styles.formCardNumber} float-right`}>{page}</span>

        <h4 className={styles.formCardTitle}>
          <FontIcon className={styles.titleIcon} value={titleicon} />
          {title}
        </h4>

        {children}

        <Content className={styles.controlsLeft}>
          {page > 1 && <PreviousButton page={page} />}
        </Content>

        <Content className={styles.controls}>
          <SubmitOrNext handleSubmit={handleSubmit} page={page} />
        </Content>
      </Content>
    </Container>
  )
}

function SubmitOrNext({ handleSubmit, page }) {
  if (handleSubmit) {
    return (
      <Button floating icon='chevron_right' onClick={handleSubmit} primary />
    )
  }

  return (
    <Link to={`project-new-${page + 1}`}>
      <Button floating icon='chevron_right' primary />
    </Link>
  )
}

function PreviousButton({ page }) {
  return (
    <Link to={`project-new-${page - 1}`}>
      <Button floating icon='chevron_left' secondary />
    </Link>
  )
}
