import React from 'react'

import { Content, Block } from 'modules/layout'
import { CurrencyInput } from 'modules/ui'
import Step from '../Step'

export default function Step5(props) {
  const { projectedLandCosts, projectedHardCosts, projectedSoftCosts } = props

  const stepProps = {
    heading: "Next let's fill out your project's investment numbers",
    title: 'Investment Numbers',
    titleicon: 'trending_up',
    page: 5
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content className='small-12 large-8'>
            <CurrencyInput
                {...projectedLandCosts}
                hint='ex. $3,000,000'
                label='Projected Land Costs' />
          </Content>

          <Content className='small-12 large-6'>
            <CurrencyInput
                {...projectedHardCosts}
                hint='ex. $ 12,000,000'
                label='Projected Hard Costs' />
          </Content>

          <Content className='small-12 large-6'>
            <CurrencyInput
                {...projectedSoftCosts}
                hint='ex. $50,000'
                label='Projected Soft Costs' />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}
