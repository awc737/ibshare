import React from 'react'

import { Block } from 'modules/layout'

import styles from './styles.scss'

export default function ProjectForm(props) {
  const { children, fields, addValue, handleSubmit } = props

  return (
    <Block className={styles.root}>
      {React.cloneElement(children, { ...fields, addValue, handleSubmit })}
    </Block>
  )
}

export Step1 from './Step1'
export Step2 from './Step2'
export Step3 from './Step3'
export Step4 from './Step4'
export Step5 from './Step5'
export Step6 from './Step6'
export Step7 from './Step7'
export Step8 from './Step8'
export Step9 from './Step9'
export Step10 from './Step10'
export Step11 from './Step11'
export Step12 from './Step12'
export Step13 from './Step13'
