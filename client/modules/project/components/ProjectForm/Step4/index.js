import React from 'react'
import { range } from 'ramda'

import { Content, Block } from 'modules/layout'
import { CurrencyInput, DocumentUploader, FontIcon } from 'modules/ui'
import Step from '../Step'

import styles from '../styles.scss'

export default function Step4(props) {
  const {
    projectedGrossProfit,
    estimatedStartQuarter,
    estimatedStartYear
  } = props

  const budget = props.budgets[0]
  const budgetName = budget && budget.name

  const stepProps = {
    heading: "Next let's fill out your project's investment numbers",
    title: 'Investment Numbers',
    titleicon: 'trending_up',
    page: 4
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block className='small-12'>
          <Content>
            <CurrencyInput
                {...projectedGrossProfit}
                hint='ex. $2,000,000'
                label='Projected Gross Profit' />
          </Content>
        </Block>

        <Block className='small-12'>
          <Content className='small-12 medium-10 large-9'>
            <label className={styles.label}>
              Estimated Start Date
            </label>

            <div className={`${styles.selectLeft} small-5`}>
              <QuarterSelect {...estimatedStartQuarter} />
              <FontIcon
                  className={styles.dropdownIcon}
                  value='keyboard_arrow_down' />
            </div>

            <div className={`${styles.selectRight} small-7`}>
              <YearSelect {...estimatedStartYear} />
              <FontIcon
                  className={styles.dropdownIcon}
                  value='keyboard_arrow_down' />
            </div>
          </Content>

          <Content className='small-12 medium-2 large-3 text-center'>
            <label className={styles.formCardLabel}>
              Upload Budget
            </label>
            {budgetName && <DocumentUploader small {...budgetName} />}
          </Content>
        </Block>
      </Content>
    </Step>
  )
}

function QuarterSelect(props) {
  return (
    <select {...props}>
      {range(1, 5).map((quarter, index) =>
        <option key={index} value={quarter}>Q{quarter}</option>
      )}
    </select>
  )
}

function YearSelect(props) {
  const startYear = new Date().getFullYear()
  const yearsToDisplay = range(startYear, startYear + 4)

  return (
    <select {...props}>
      {yearsToDisplay.map((year, index) =>
        <option key={index} value={year}>{year}</option>
      )}
    </select>
  )
}
