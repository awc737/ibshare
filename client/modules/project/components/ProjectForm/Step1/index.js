import React from 'react'

import { Content, Block } from 'modules/layout'
import { Input } from 'modules/ui'
import Step from '../Step'

export default function Step1({ title, legalEntityName, location }) {
  const stepProps = {
    heading: "Let's start with your project's basic information",
    title: 'Basic Information',
    titleicon: 'insert_drive_file',
    page: 1
  }

  return (
    <Step {...stepProps}>
      <Content className='small-10 small-offset-1'>
        <Block>
          <Content className='large-6'>
            <Input
                {...title}
                label='Project Title'
                hint='ex. Park Place Development' />
          </Content>

          <Content className='large-6'>
            <Input
                {...legalEntityName}
                label='Legal Entity Name'
                hint='ex. Capital First Ventures' />
          </Content>
        </Block>

        <Block>
          <Content className='large-9'>
            <Input
                {...location}
                label='Location'
                hint='ex. Brooklyn, NY' />
          </Content>
        </Block>
      </Content>
    </Step>
  )
}
