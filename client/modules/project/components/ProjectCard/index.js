import React from 'react'
import { Link } from 'react-router-named-routes'

import { projectedValue } from '../../models/Project'
import { Button, Card } from 'modules/ui'
import { Content } from 'modules/layout'
import FeaturedImage from 'modules/project/components/Project/FeaturedImage'
import FundingProgress from '../../containers/FundingProgress'
import { filterByIds, formatCurrency } from 'utils'

import styles from './styles.scss'

export default function ProjectCard({ project, media }) {
  return (
    <Content key={project.id} className='small-12 medium-6 large-4'>
      <Link to='project' params={{ id: project.id }}>
        <Card className={styles.card}>
          <FeaturedImage
              media={filterByIds(project.media, media)}
              className={styles.cardMedia} />
          <div className={styles.cardContent}>
            <h3 className={styles.type}>
              {project.typeOfDevelopment}
            </h3>

            <div className={styles.location}>
              {project.location}
            </div>

            <h2 className={styles.title}>
              {project.title}
            </h2>

            <div className={styles.legalEntity}>
              {project.legalEntityName}
            </div>

            <div className={styles.fundingWrapper}>
              <FundingProgress project={project} />
            </div>

            <Attribute
                currency
                label='Gross Profit'
                value={projectedValue(project)} />
            <Attribute
                currency
                label='Minimum Investment'
                value={project.minimumInvestment} />
            <Attribute
                label='Estimated Start Date'
                value={startDate(project)} />
            <Attribute
                label='Investment Term'
                value={formatTerm(project)} />
            <Attribute
                label='Annual ROI'
                value={(project.annualRoi || '') + '%'} />

            <div className={styles.moreButton}>
              <Button
                  icon='chevron_right'
                  floating
                  primary
                  mini />
            </div>
          </div>
        </Card>
      </Link>
    </Content>
  )
}

function Attribute({ label, value, currency }) {
  return (
    <div className={styles.projectAttribute}>
      <span className={styles.label}>{label}:</span>
      <span className={styles.value}>
        &nbsp;
        {currency ? formatCurrency(value) : value}
      </span>
    </div>
  )
}

function formatTerm(project) {
  const term = project.investmentTerm || ''

  return term + ' months'
}

function startDate(project) {
  return `Q${project.estimatedStartQuarter} ${project.estimatedStartYear}`
}
