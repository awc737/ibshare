import React from 'react'

import { Content } from 'modules/layout'
import { OfferForm } from 'modules/offer'
import { formatCurrency } from 'utils'
import FundingProgress from '../../../containers/FundingProgress'

import styles from './styles.scss'

export default function Deal({ project }) {
  return (
    <Content>
      <Content className={styles.fundingWrapper}>
        <FundingProgress
            project={project}
            hasProgressBar
            large />
      </Content>

      <Content className={styles.terms}>
        <div className={styles.termTitle}>
          Projected Gross Profit:
          <span className={styles.termValue}>
            &nbsp;{formatCurrency(parseFloat(project.projectedGrossProfit))}
          </span>
        </div>

        <div className={styles.termTitle}>
          Est. Project Start:
          <span className={styles.termValue}>
            &nbsp;Q{project.estimatedStartQuarter}
            ,&nbsp;{project.estimatedStartYear}
          </span>
        </div>

        <div className={styles.termTitle}>
          Investment Term:
          <span className={styles.termValue}>
            &nbsp;{project.investmentTerm}&nbsp;months
          </span>
        </div>

        <div className={styles.termTitle}>
          Annual ROI:
          <span className={styles.termValue}>
            &nbsp;{project.annualRoi}%
          </span>
        </div>
      </Content>

      <Content>
        <OfferForm projectId={project.id} />
      </Content>
    </Content>
  )
}
