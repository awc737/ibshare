import React from 'react'

import { Block } from 'modules/layout'
import Header from './Header'
import Detail from './Detail'
import Deal from './Deal'

import styles from './styles.scss'

export default function Project(props) {
  return (
    <div className={styles.root}>
      <Header {...props} />

      <Block className={styles.wrapper} wrap>
        <Block className='small-12 medium-8'>
          <Detail {...props} />
        </Block>

        <Block vertical className='small-12 medium-4'>
          <h2 className={styles.sidebarTitle}>Deal Details</h2>
          <Deal {...props} />
        </Block>
      </Block>
    </div>
  )
}
