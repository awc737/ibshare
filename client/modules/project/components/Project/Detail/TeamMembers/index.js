import React from 'react'

import { Block, Content } from 'modules/layout'
import { Avatar } from 'modules/ui'

import styles from './styles.scss'

export default function TeamMembers({ members = [] }) {
  return (
    <Block>
      {members.reverse().map(teamMember =>
        <TeamMember key={teamMember.id} teamMember={teamMember} />
      )}
    </Block>
  )
}

function TeamMember({ teamMember }) {
  return (
    <Content className='medium-12 large-6'>
      <div className='text-center'>
        <Avatar
            className={styles.avatar}
            icon='photo_camera'
            image={teamMember.publicUrl} />
      </div>

      <div className={styles.name}>
        {teamMember.name}

        <span className={styles.title}>
          {teamMember.title}
        </span>
      </div>

      <p className={styles.bio}>
        {teamMember.bio}
      </p>
    </Content>
  )
}
