export default function filterMediaByExtension(extensions, media) {
  return media.filter(file => fileExtFilterRegExp(extensions)
    .test(file.publicUrl))
}

function fileExtFilterRegExp(extensions) {
  let ext = extensions

  if (typeof extensions === 'string') {
    ext = [extensions]
  }

  return new RegExp('\.(' + ext.join('|') + ')\$')
}
