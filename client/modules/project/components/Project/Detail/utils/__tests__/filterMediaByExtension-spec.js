import { expect } from 'chai'

import filterMediaByExtension from '../filterMediaByExtension'

describe('filterMediaByExtension', () => {
  const jpgImage = { publicUrl: 'image.jpg' }
  const jpegImage = { publicUrl: 'image.jpeg' }
  const pngImage = { publicUrl: 'image.png' }
  const movFile = { publicUrl: 'movie.mov' }

  const testMedia = [
    jpgImage,
    jpegImage,
    pngImage,
    movFile
  ]

  it('includes only files matching a single extension', () => {
    const media = filterMediaByExtension('png', testMedia)

    expect(media.length).to.eq(1)
    expect(media).to.include(pngImage)
  })

  it('includes only files matching multiple extensions', () => {
    const media = filterMediaByExtension(['png', 'jpg'], testMedia)

    expect(media.length).to.eq(2)
    expect(media).to.include(pngImage, jpgImage)
  })

  it('allows regexp as a file extension', () => {
    const media = filterMediaByExtension('jpe?g', testMedia)

    expect(media.length).to.eq(2)
    expect(media).to.include(jpgImage, jpegImage)
  })

  it('returns an empty array when no files are matching an extension', () => {
    const media = filterMediaByExtension(['not_valid'], testMedia)

    expect(media.length).to.eq(0)
  })
})
