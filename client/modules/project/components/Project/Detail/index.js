import React from 'react'

import { Block, Content } from 'modules/layout'
import Slider from './Slider'
import TeamMembers from './TeamMembers'
import filterMediaByExtension from './utils/filterMediaByExtension'

import styles from './styles.scss'

const validImageExtensions = ['jpe?g', 'png']

export default function Details({ media, project, teamMembers }) {
  const images = filterMediaByExtension(validImageExtensions, media)
    .map(image => ({ src: image.publicUrl }))

  return (
    <Block vertical className={styles.root}>
      <Content>
        <Slider images={images} />
      </Content>

      <Block align='center' className={styles.main}>
        <Content className='small-12 medium-10'>
          <h3 className={styles.heading}>
            Project Description
          </h3>

          <p className={styles.detail}>
            {project.summary}
          </p>

          <p className={styles.detail}>
            {project.propertyDetails}
          </p>

          <h3 className={styles.sectionTitle}>
            Financial Summary
          </h3>

          <p className={styles.detail}>
            {project.financialSummary}
          </p>

          <h3 className={styles.sectionTitle}>
            Market Summary
          </h3>

          <p className={styles.detail}>
            {project.marketSummary}
          </p>

          <h3 className={styles.sectionTitle}>
            Investment Offering
          </h3>

          <p className={styles.detail}>
            {project.investmentOffering}
          </p>

          <h3 className={styles.teamTitle}>
            The Team
          </h3>

          <TeamMembers members={teamMembers} />
        </Content>
      </Block>
    </Block>
  )
}
