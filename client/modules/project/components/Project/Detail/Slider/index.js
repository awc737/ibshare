import React, { Component } from 'react'
import Slick from 'react-slick'
import Lightbox from 'react-images'

import styles from './styles.scss'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const settings = {
  arrows: true,
  dots: false,
  draggable: false,
  infinite: false,
  slidesToScroll: 1,
  slidesToShow: 4,
  speed: 500
}

/* eslint-disable react/no-set-state */
export default class Slider extends Component {
  constructor() {
    super()

    this.handleClose = this.handleClose.bind(this)
    this.handleNext = this.handleNext.bind(this)
    this.handlePrevious = this.handlePrevious.bind(this)

    this.state = {
      lightBoxIsOpen: false,
      lightboxIndex: 1
    }
  }

  handleOpen(index) {
    this.setState({
      lightBoxIsOpen: true,
      lightboxIndex: index
    })
  }

  handleClose() {
    this.setState({
      lightBoxIsOpen: false
    })
  }

  handleNext() {
    this.setState({
      lightboxIndex: this.state.lightboxIndex + 1
    })
  }

  handlePrevious() {
    this.setState({
      lightboxIndex: this.state.lightboxIndex - 1
    })
  }

  render() {
    const { images } = this.props
    const { lightboxIndex, lightBoxIsOpen } = this.state

    return (
      <div>
        <Slick className={styles.slider} {...settings}>
          {images.map((image, index) =>
            <div key={index}>
              <span
                  className={styles.preview}
                  onClick={this.handleOpen.bind(this, index)}
                  style={{ backgroundImage: `url('${image.src}')` }}></span>
            </div>
          )}
        </Slick>

        <Lightbox
            currentImage={lightboxIndex}
            images={images}
            isOpen={lightBoxIsOpen}
            onClickNext={this.handleNext}
            onClickPrev={this.handlePrevious}
            onClose={this.handleClose} />
      </div>
    )
  }
}
