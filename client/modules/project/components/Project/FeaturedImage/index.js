import React from 'react'

import { filter, prop } from 'ramda'

export default function FeaturedImage({ media, className }) {
  const imageUrl = featuredImageUrl(media)

  return imageUrl
    ? <img src={imageUrl} className={className} />
    : <div className={className} />
}

function featuredImageUrl(media) {
  const [featured = {}] = filter(prop('featured'), media)
  const [first = {}] = media

  return featured.publicUrl || first.publicUrl
}
