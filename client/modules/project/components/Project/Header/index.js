import React from 'react'
import { Container, Block, Content } from 'modules/layout'
import { FontIcon } from 'modules/ui'

import FeaturedImage from 'modules/project/components/Project/FeaturedImage'

import styles from './styles.scss'

export default function Header({ project, media }) {
  return (
    <Block vertical className={styles.root}>
      <div className={styles.background}>
        <FeaturedImage className={styles.cardMedia} media={media} />
      </div>

      <Block className={styles.information}>
        <Content className={styles.contentRoot}>
          <h1 className={styles.title}>
            {project.title}
          </h1>

          <h2 className={styles.location}>
            <FontIcon className={styles.locationIcon} value='place' />
            {project.location}
          </h2>

          <Container className={styles.details}>
            <Block className='small-up-1 medium-up-3'>
              <Content>
                <FontIcon className={styles.typeIcon} value='location_city' />

                <div className={styles.typeOfDevelopment}>
                  {project.typeOfDevelopment}
                </div>
              </Content>

              <Content>
                <div className={styles.detailValue}>
                  {project.numberOfUnits}
                </div>

                <div className={styles.detailLabel}>
                  units
                </div>
              </Content>

              <Content>
                <div className={styles.detailValue}>
                  {formatAcres(project.numberOfAcres)}
                </div>

                <div className={styles.detailLabel}>
                  acres
                </div>
              </Content>
            </Block>
          </Container>
        </Content>
      </Block>

      <Content shrink className={styles.downIcon}>
        <FontIcon value='expand_more' />
      </Content>
    </Block>
  )
}

function formatAcres(string) {
  if (!string) return ''

  return formatFloat(parseFloat(string))
}

function formatFloat(number) {
  const isRound = number % 1 === 0

  return isRound ? number.toFixed(0) : number.toFixed(2)
}
