import React from 'react'
import { values } from 'ramda'

import { Block, Content } from 'modules/layout'
import ProjectCard from '../ProjectCard'

import styles from './styles.scss'

export default function ProjectList({ projects, media }) {
  return (
    <Block className={styles.root} vertical>
      <Block wrap>
        <Content className='small-12'>
          <h1 className={styles.pageTitle}>
            Investment Opportunities |
            <span className={styles.projectCount}>
              &nbsp;{values(projects).length} Results
            </span>
          </h1>
        </Content>

        {values(projects).map(project =>
          <ProjectCard key={project.id} project={project} media={media} />
        )}
      </Block>
    </Block>
  )
}
