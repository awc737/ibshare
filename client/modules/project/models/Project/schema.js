import { normalize, Schema, arrayOf } from 'normalizr'

import { schema as documents } from '../Document'
import { schema as media } from '../Media'
import { schema as teamMembers } from '../TeamMember'

const projects = new Schema('projects')

projects.define({ documents: arrayOf(documents) })
projects.define({ media: arrayOf(media) })
projects.define({ teamMembers: arrayOf(teamMembers) })

export default data => normalize(data, { projects: arrayOf(projects) })
