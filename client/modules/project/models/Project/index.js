import R, { curry, map, pick, pipe, reduce, values } from 'ramda'

import { updateProp } from 'utils'

// developmentTypes :: [String]
export const developmentTypes = [
  'Single Family – For Sale',
  'Multi-Family – For Sale',
  'Apartments – For Rent',
  'Apartments – For Sale',
  'Hotel / Motel',
  'Mixed Use – Live/Work, Multi w/ Commercial',
  'Senior Housing – Independent, Assisted & Continuing Care',
  'Student Housing - Dormitory',
  'Manufactured Housing',
  'Health Care'
]

export normalize from './schema'

// add :: Project -> {Project} -> {Project}
export const add = curry(
  (project, projects) => ({ ...projects, [project.id]: project })
)

// find :: String -> {Project} -> Project
export const find = curry(
  (id, projects) => projects[id] || {}
)

// update :: (Project -> Project) -> String -> {Project} -> {Project}
export const update = curry(
  (fn, id, projects) => updateProp(id)(fn, projects)
)

// projectedValue :: Project -> Number
export const projectedValue = pipe(
  pick([
    'projectedLandCosts',
    'projectedHardCosts',
    'projectedSoftCosts',
    'projectedGrossProfit'
  ]),
  values,
  map(parseInt),
  reduce(R.add, 0)
)
