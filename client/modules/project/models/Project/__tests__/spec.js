import { expect } from 'chai'

import { normalize, projectedValue } from '..'

describe('normalize', () => {
  describe('with no child collections', () => {
    const { entities, result } = normalize({
      projects: [
        { id: 1, title: 'Foo' },
        { id: 2, title: 'Bar' }
      ]
    })

    it('captures the right ids', () => {
      expect(result.projects).to.eql([1, 2])
    })

    it('converts from array to object with id keys', () => {
      expect(entities.projects).to.eql({
        1: { id: 1, title: 'Foo' },
        2: { id: 2, title: 'Bar' }
      })
    })
  })

  describe('with nested documents', () => {
    const { entities } = normalize({
      projects: [
        {
          id: 1,
          title: 'Foo',
          documents: [{ id: 1, name: 'A' }, { id: 2, name: 'B' }]
        }, {
          id: 2,
          title: 'Bar',
          documents: [{ id: 3, name: 'C' }, { id: 4, name: 'D' }]
        }
      ]
    })

    it('references document ids on each project', () => {
      expect(entities.projects).to.eql({
        1: { id: 1, title: 'Foo', documents: [1, 2] },
        2: { id: 2, title: 'Bar', documents: [3, 4] }
      })
    })

    it('consolidates documents into their own collection', () => {
      expect(entities.documents).to.eql({
        1: { id: 1, name: 'A' },
        2: { id: 2, name: 'B' },
        3: { id: 3, name: 'C' },
        4: { id: 4, name: 'D' }
      })
    })
  })
})

describe('projectedValue', () => {
  it('derives the projected value from the costs and est. profit', () => {
    const project = {
      someOtherProperty: 4,
      projectedLandCosts: '1',
      projectedHardCosts: 1,
      projectedSoftCosts: 1,
      projectedGrossProfit: 1
    }

    expect(projectedValue(project)).to.eql(4)
  })
})
