export const allMedia = state => state.app.project.media
export const allProjects = state => state.app.project.projects
export const allTeamMembers = state => state.app.project.teamMembers
