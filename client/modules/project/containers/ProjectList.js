import { connect } from 'react-redux'

import ProjectList from '../components/ProjectList'
import { allMedia, allProjects } from '../selectors'

function mapStateToProps(state) {
  return {
    projects: allProjects(state),
    media: allMedia(state)
  }
}

export default connect(mapStateToProps)(ProjectList)
