import { connect } from 'react-redux'

import ProjectComponent from '../components/Project'
import * as Project from '../models/Project'
import { allMedia, allProjects, allTeamMembers } from '../selectors'
import { filterByIds } from 'utils'

function mapStateToProps(state, { params: { id } }) {
  const project = Project.find(id, allProjects(state))
  const media = filterByIds(project.media, allMedia(state))
  const teamMembers = filterByIds(project.teamMembers, allTeamMembers(state))

  return {
    project,
    media,
    teamMembers
  }
}

export default connect(mapStateToProps)(ProjectComponent)
