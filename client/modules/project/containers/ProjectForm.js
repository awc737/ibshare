import { reduxForm, addArrayValue } from 'redux-form'
import { partial } from 'ramda'

import { addProject } from '../actions'
import ProjectForm from '../components/ProjectForm'
import { config } from '../form'

const name = 'project'

export default reduxForm(config(name), null, {
  onSubmit: addProject,
  addValue: partial(addArrayValue, [name])
})(ProjectForm)
