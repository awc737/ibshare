import { connect } from 'react-redux'

import FundingProgress from '../components/FundingProgress'
import { totalForProject } from 'modules/offer'

function mapStateToProps(state, { project }) {
  return {
    required: project.totalInvestmentRequired,
    raised: totalForProject(project.id, state)
  }
}

export default connect(mapStateToProps)(FundingProgress)
