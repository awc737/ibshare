import { connect } from 'react-redux'

import Layout from '../components/Layout'
import { fetchProjects } from '../actions'
import { fetchOffers } from 'modules/offer'
import { fetchUser } from 'modules/user'

export default connect(null, { fetchUser, fetchProjects, fetchOffers })(Layout)
