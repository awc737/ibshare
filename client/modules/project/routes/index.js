import React from 'react'
import { IndexRedirect, IndexRoute, Route } from 'react-router'

import Layout from '../containers/Layout'
import Project from '../containers/Project'
import ProjectForm from '../containers/ProjectForm'
import {
  Step1,
  Step2,
  Step3,
  Step4,
  Step5,
  Step6,
  Step7,
  Step8,
  Step9,
  Step10,
  Step11,
  Step12,
  Step13
} from '../components/ProjectForm'
import ProjectList from '../containers/ProjectList'

export default (
  <Route name='projects' path='projects' component={Layout}>
    <IndexRoute component={ProjectList} />

    <Route name='project-new' path='new' component={ProjectForm}>
      <IndexRedirect to='1' />

      <Route name='project-new-1' path='1' component={Step1} />
      <Route name='project-new-2' path='2' component={Step2} />
      <Route name='project-new-3' path='3' component={Step3} />
      <Route name='project-new-4' path='4' component={Step4} />
      <Route name='project-new-5' path='5' component={Step5} />
      <Route name='project-new-6' path='6' component={Step6} />
      <Route name='project-new-7' path='7' component={Step7} />
      <Route name='project-new-8' path='8' component={Step8} />
      <Route name='project-new-9' path='9' component={Step9} />
      <Route name='project-new-10' path='10' component={Step10} />
      <Route name='project-new-11' path='11' component={Step11} />
      <Route name='project-new-12' path='12' component={Step12} />
      <Route name='project-new-13' path='13' component={Step13} />
    </Route>

    <Route name='project' path=':id' component={Project} />
  </Route>
)
