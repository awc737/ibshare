import ActionTypes from '../constants/ActionTypes'
import { Project } from '../models'
import { createReducer, updateProp } from 'utils'

const initialState = {
  projects: {}
}

const handlers = {
  [ActionTypes.CREATE_PROJECT_SUCCESS]: addProject,
  [ActionTypes.FETCH_PROJECTS_SUCCESS]: fetchProjects
}

export default createReducer(initialState, handlers)

const updateProjects = updateProp('projects')

function addProject(state, { payload }) {
  return updateProjects(Project.add(payload.project), state)
}

function fetchProjects(state, { payload }) {
  const { entities } = Project.normalize({ projects: payload })

  return { ...state, ...entities }
}
