import { expect } from 'chai'

import { toServer } from '..'

describe('toServer', () => {
  it('removes invalid budgets', () => {
    const formData = { budgets: [{ title: 'Budget' }] }

    expect(toServer(formData)).to.eql({})
  })

  it('does not remove valid budgets', () => {
    const formData = { budgets: [{ title: 'Budget', name: 'my.file' }] }

    expect(toServer(formData)).to.eql(formData)
  })

  it('removes empty collections', () => {
    const formData = { title: 'Foo', documents: [] }

    expect(toServer(formData)).to.eql({ title: 'Foo' })
  })

  it('removes currency formatting', () => {
    const formData = { totalInvestmentRequired: '$200,000' }

    expect(toServer(formData)).to.eql({ totalInvestmentRequired: 200000 })
  })
})
