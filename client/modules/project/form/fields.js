const budgetFields = [
  'budgets[].name',
  'budgets[].title'
]

const documentFields = [
  'documents[].name',
  'documents[].title'
]

const mediaFields = [
  'media[].name',
  'media[].title',
  'media[].featured'
]

const teamMemberFields = [
  'teamMembers[].avatar',
  'teamMembers[].bio',
  'teamMembers[].name',
  'teamMembers[].previewUrl',
  'teamMembers[].title'
]

export const currencyFields = [
  'minimumInvestment',
  'projectedGrossProfit',
  'projectedValue',
  'projectedHardCosts',
  'projectedLandCosts',
  'projectedSoftCosts',
  'totalDebtRequired',
  'totalInvestmentRequired'
]

export default [
  'annualRoi',
  'createdAt',
  'estimatedStartQuarter',
  'estimatedStartYear',
  'featuredImage',
  'financialSummary',
  'fundedSoFar',
  'id',
  'investmentOffering',
  'investmentTerm',
  'legalEntityName',
  'location',
  'marketSummary',
  'numberOfAcres',
  'numberOfBuildings',
  'numberOfUnits',
  'propertyDetails',
  'summary',
  'title',
  'typeOfDevelopment',
  ...budgetFields,
  ...currencyFields,
  ...documentFields,
  ...mediaFields,
  ...teamMemberFields
]
