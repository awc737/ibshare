import fields from './fields'

const initialValues = {
  typeOfDevelopment: '',
  estimatedStartYear: new Date().getFullYear(),
  estimatedStartQuarter: 1,
  budgets: [{ title: 'Budget' }]
}

export default function(name) {
  return { fields, initialValues, ...{ form: name } }
}
