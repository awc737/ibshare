import {
  evolve,
  isEmpty,
  map,
  merge,
  pick,
  pipe,
  reject
} from 'ramda'

import { currencyFields } from './fields'
import { unformatCurrency } from 'utils'

export config from './config'

const unformatCurrencies = project => merge(
  project,
  map(unformatCurrency, pick(currencyFields, project))
)

const removeInvalidBudget = evolve({
  budgets: budget => budget[0].name ? budget : []
})

// toServer :: Object -> API
export const toServer = pipe(
  unformatCurrencies,
  removeInvalidBudget,
  reject(isEmpty)
)
