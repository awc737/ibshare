import { createActionTypes } from 'utils'

export default createActionTypes('project', [
  'CREATE_PROJECT_REQUEST',
  'CREATE_PROJECT_SUCCESS',

  'FETCH_PROJECTS_REQUEST',
  'FETCH_PROJECTS_SUCCESS',

  'API_FAILURE'
])
