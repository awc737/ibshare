import { createActionTypes } from 'utils'

export default createActionTypes('offer', [
  'CREATE_OFFER_REQUEST',
  'CREATE_OFFER_SUCCESS',

  'FETCH_OFFERS_REQUEST',
  'FETCH_OFFERS_SUCCESS',

  'API_FAILURE'
])
