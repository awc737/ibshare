import ActionTypes from '../constants/ActionTypes'
import { Offer } from '../models'
import { createReducer, updateProp } from 'utils'

const initialState = {
  offers: []
}

const handlers = {
  [ActionTypes.CREATE_OFFER_SUCCESS]: addOffer,
  [ActionTypes.FETCH_OFFERS_SUCCESS]: fetchOffers
}

export default createReducer(initialState, handlers)

const updateOffers = updateProp('offers')

function addOffer(state, { payload: { offer } }) {
  return updateOffers(Offer.add(offer), state)
}

function fetchOffers(state, { payload }) {
  const { entities } = Offer.normalize({ offers: payload })

  return { ...entities }
}
