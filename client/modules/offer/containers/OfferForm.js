import { reduxForm } from 'redux-form'

import OfferForm from '../components/OfferForm'
import { makeOffer } from '../actions'
import { config } from '../form'
import { totalForProjectByUser } from '../selectors'

const name = 'offer'

function mapStateToProps(state, props) {
  return {
    ...props,
    initialValues: { projectId: props.projectId },
    offered: totalForProjectByUser(props.projectId, state)
  }
}

export default reduxForm(config(name), mapStateToProps, {
  onSubmit: makeOffer
})(OfferForm)
