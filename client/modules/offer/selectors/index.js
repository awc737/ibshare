import { compose } from 'ramda'

import { currentUser } from 'modules/user'
import { amountsTotaled, byUser, forProject } from '../models/Offer'

const allOffers = state => state.app.offer.offers

export function totalForProject(projectId, state) {
  const offers = compose(forProject(projectId), allOffers)

  return amountsTotaled(offers(state))
}

export function totalForProjectByUser(projectId, state) {
  const userId = currentUser(state).id
  const offers = compose(forProject(projectId), byUser(userId), allOffers)

  return amountsTotaled(offers(state))
}
