import ActionTypes from '../constants/ActionTypes'
import { toServer } from '../form'
import { callApi } from 'utils'

export function fetchOffers() {
  return callApi({
    endpoint: '/api/offers',
    types: [
      ActionTypes.FETCH_OFFERS_REQUEST,
      ActionTypes.FETCH_OFFERS_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  })
}

export function makeOffer(data) {
  return callApi({
    endpoint: '/api/offers',
    method: 'POST',
    body: { offer: toServer(data) },
    types: [
      ActionTypes.CREATE_OFFER_REQUEST,
      ActionTypes.CREATE_OFFER_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  })
}
