import { evolve, pick, pipe } from 'ramda'

import { unformatCurrency } from 'utils'

const fields = [
  'amount',
  'projectId'
]

const initialValues = {}

export function config(name) {
  return { fields, initialValues, ...{ form: name } }
}

// toServer :: Object -> Offer
export const toServer = pipe(
  pick(fields),
  evolve({ amount: unformatCurrency })
)
