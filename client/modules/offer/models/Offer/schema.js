import { normalize, Schema, arrayOf } from 'normalizr'

const offers = new Schema('offers')

export default data => normalize(data, { offers: arrayOf(offers) })
