import R, { compose, curry, pickBy, map, prop, reduce, values } from 'ramda'

export normalize from './schema'

// add :: Offer -> {Offer} -> {Offer}
export const add = curry((offer, offers) =>
  ({ ...offers, [offer.id]: offer })
)

// forProject :: String -> {Offer} -> {Offer}
export const forProject = curry((projectId, offers) =>
  pickBy(value => value.projectId === projectId, offers)
)

// byUser :: String -> {Offer} -> {Offer}
export const byUser = curry((userId, offers) =>
  pickBy(value => value.userId === userId, offers)
)

// amounts :: [Offer] -> [Number]
const amounts = map(compose(parseInt, prop('amount')))

// amountsTotaled :: {Offer} -> Number
export const amountsTotaled = compose(reduce(R.add, 0), amounts, values)
