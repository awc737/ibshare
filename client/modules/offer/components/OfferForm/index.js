import React from 'react'

import { Button, CurrencyInput } from 'modules/ui'
import { formatCurrency } from 'utils'

import styles from './styles.scss'

export default function OfferForm(props) {
  const { fields: { amount }, handleSubmit, offered, submitting } = props

  return (
    <div>
      <CurrencyInput
          {...amount}
          label='Indication of Interest Amount'
          hint='ex. $50,000' />

      {Boolean(offered) &&
        <div className={styles.notification}>
          <p>
            {`You've successfully submitted your interest on this project to
            invest ${formatCurrency(offered)}!`}
          </p>
        </div>
      }

      <Button
          primary
          raised
          disabled={submitting}
          className={styles.button}
          label='Submit Interest'
          onClick={handleSubmit} />

      <div className={styles.disclaimer}>
        <p>
          {`Any action taken in connection with the “Submit Interest” option
          will be considered a non-binding expression of interest.`}
        </p>

        <p>
          {`Investment opportunities will be prioritized in the order in which
          Investor's expressions of interest are received. There is no guarantee
          your investment will ultimately be accepted.`}
        </p>

        <p>
          {`Infobrij is not a registered or licensed broker, dealer,
          broker-dealer, investment adviser, or investment manager.`}
        </p>
      </div>
    </div>
  )
}
