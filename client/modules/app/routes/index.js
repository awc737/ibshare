import React from 'react'
import { IndexRedirect, Route } from 'react-router'

import AppContainer from '../containers/App'
import { UserRoutes } from 'modules/user'
import { ProjectRoutes } from 'modules/project'

const routes = (
  <Route name='root' path='/' component={AppContainer}>
    <IndexRedirect to='projects' />

    {UserRoutes}
    {ProjectRoutes}
  </Route>
)

export default routes
