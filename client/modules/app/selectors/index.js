export function pathname(state) {
  return state.routing.locationBeforeTransitions.pathname
}
