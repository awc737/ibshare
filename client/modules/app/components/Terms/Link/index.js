import React from 'react'

import styles from './styles.scss'

export default function Link() {
  return (
    <label className={styles.root}>
      &nbsp;Terms of Service and Privacy Policy&nbsp;
    </label>
  )
}
