import React from 'react'

import { Modal } from 'modules/layout'
import Link from './Link'
import Content from './Content'

export default function Terms() {
  return <Modal link={<Link />} content={<Content />} />
}
