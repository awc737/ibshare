import React from 'react'

import { logo } from 'modules/app'
import styles from './styles.scss'

export default function Content() {
  return (
    <div className={styles.contentWrapper}>
      <div className={styles.brand}>
        <img className={styles.logo} src={logo} alt='Infobrij logo' />
      </div>
      <h1 className={styles.title}>Terms &amp; Conditions</h1>
      <p>
        {`Infobrij.com is a website operated by Infobrij LLC (“Infobrij”), and
        by accessing this website, you agree to be bound by the following:`}
      </p>
      <p>
        {`Infobrij is not a registered or licensed broker, dealer,
        broker-dealer, investment adviser, investment manager, or funding portal
        in any jurisdiction, nor does Infobrij engage in any activities that
        would require any such registration. If any such services are required,
        securities will be offered through a duly registered broker/dealer. The
        services provided by Infobrij are intended for accredited investors only
        who are familiar with and willing to accept the risks associated with
        private investments. Any securities sold through private placements are
        not publicly traded and are intended for investors who do not have a
        need for liquidity in their investment.`}
      </p>
      <p>
        {`Infobrij does not endorse any security, and its services to or
        statements about its sponsors or clients should never be construed as
        any endorsement of or opinion about any security of any sponsor or
        client or the prospects of future performance.`}
      </p>
      <p>
        {`Unless otherwise agreed in writing, Infobrij does not verify, and
        makes no warranty, express or implied, regarding, the accuracy or
        completeness of information concerning its sponsors, clients or service
        providers. Neither Infobrij nor any of its managers, members, employees,
        representatives, affiliates, or agents shall have any liability arising
        from or relating to any inaccuracy or incompleteness of any fact or
        opinion in any materials or communications regarding any users of its
        platform or others.`}
      </p>
    </div>
  )
}
