import React from 'react'
import { Link } from 'react-router-named-routes'

import logo from './infobrij-logo.png'
import styles from './styles.scss'

export default function Navigation({ signOut }) {
  return (
    <div className={styles.navBar}>
      <Link className={styles.homeLink} to='projects'>
        <img className={styles.logo} src={logo} alt='Infobrij logo' />
      </Link>

      <ul className={styles.navLinks}>
        <li className={styles.navLinkItem}>
          <Link onlyActiveOnIndex activeClassName={styles.active} to='projects'>
            Investment Opportunities
          </Link>
        </li>

        <li className={styles.navLinkItem}>
          <a onClick={signOut}>
            Logout
          </a>
        </li>
      </ul>
    </div>
  )
}
