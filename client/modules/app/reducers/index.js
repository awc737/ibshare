import { combineReducers } from 'redux'

import { OfferReducer } from 'modules/offer'
import { ProjectReducer } from 'modules/project'
import { UserReducer } from 'modules/user'

export default combineReducers({
  offer: OfferReducer,
  project: ProjectReducer,
  user: UserReducer
})
