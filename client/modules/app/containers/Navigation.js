import { connect } from 'react-redux'

import Navigation from '../components/Navigation'
import { pathname } from '../selectors'
import { signOut } from 'modules/user'

function mapStateToProps(state) {
  return { location: pathname(state) }
}

export default connect(mapStateToProps, { signOut })(Navigation)
