import React, { Component } from 'react'
import ReactModal from 'react-modal'

import styles from './styles.scss'

/* eslint-disable react/no-set-state */
export default class Modal extends Component {
  constructor() {
    super()

    this.state = {
      modalIsOpen: false
    }
  }

  handleOpen() {
    this.setState({
      modalIsOpen: true
    })
  }

  handleClose() {
    this.setState({
      modalIsOpen: false
    })
  }

  render() {
    const { content, title, link } = this.props
    const modalContainer = document.getElementById('root')

    ReactModal.setAppElement(modalContainer)

    return (
      <div className={styles.root}>
        <span onClick={this.handleOpen.bind(this)}>
          {link}
        </span>

        <ReactModal
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.handleClose.bind(this)} >

          {title && <h2 className={styles.title}>title</h2>}

          <button
              className={styles.closeButton}
              onClick={this.handleClose.bind(this)}>
            Close
          </button>

          <div>{content}</div>
        </ReactModal>
      </div>
    )
  }
}
