import React from 'react'
import { min } from 'ramda'

import styles from './styles.scss'

export default function ProgressBar({ complete }) {
  const width = min(100, complete) + '%'

  return (
    <div className={styles.root}>
      <div className={styles.bar} style={{ width }}></div>

      <span className={styles.percent}>
        {complete}%
      </span>
    </div>
  )
}
