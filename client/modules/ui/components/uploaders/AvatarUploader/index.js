import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Avatar } from 'modules/ui'
import Uploader from '../Uploader'

import styles from './styles.scss'

export default class AvatarUploader extends Component {
  componentDidMount() {
    this.input = ReactDOM.findDOMNode(this).getElementsByTagName('input')[0]
  }

  handleClick() {
    this.input.click()
  }

  handleFinish({ fileName, signedUrl }) {
    const previewUrl = signedUrl.split('?')[0]

    this.props.previewUrl.onChange(previewUrl)
    this.props.avatar.onChange(fileName)
  }

  render() {
    const { avatar, previewUrl } = this.props

    return (
      <div className={styles.root}>
        <Avatar
            className={styles.avatar}
            icon='photo_camera'
            image={previewUrl.value}
            onClick={this.handleClick.bind(this)} />

        <Uploader name={avatar.name} onFinish={this.handleFinish.bind(this)} />
      </div>
    )
  }
}
