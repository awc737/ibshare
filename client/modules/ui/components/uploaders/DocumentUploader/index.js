import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { isNil } from 'ramda'

import Uploader from '../Uploader'
import UploaderButton from '../UploaderButton'

import styles from './styles.scss'

/* eslint-disable react/no-set-state */
export default class DocumentUploader extends Component {
  constructor() {
    super()

    this.state = { loading: false }
  }

  componentDidMount() {
    const { autoPrompt, value } = this.props

    this.input = ReactDOM.findDOMNode(this).getElementsByTagName('input')[0]

    if (autoPrompt && !value) this.input.click()
  }

  handleClick() {
    this.input.click()
  }

  handleError() {
    this.setState({
      loading: false
    })
  }

  handleFinish({ fileName, originalName }) {
    const { setTitle, onChange } = this.props

    this.setState({
      loading: false
    })

    onChange(fileName)

    if (this.shouldSetTitle()) setTitle(stripExtension(originalName))
  }

  handleProgress() {
    this.setState({
      loading: true
    })
  }

  shouldSetTitle() {
    const { title, setTitle } = this.props

    return Boolean(isNil(title) && setTitle)
  }

  render() {
    const { name, icon, small, value } = this.props
    const { loading } = this.state

    return (
      <div className={styles.root}>
        <UploaderButton
            complete={Boolean(value)}
            small={small}
            icon={icon}
            loading={loading}
            onClick={this.handleClick.bind(this)} />
        <Uploader
            name={name}
            onError={this.handleError.bind(this)}
            onFinish={this.handleFinish.bind(this)}
            onProgress={this.handleProgress.bind(this)} />
      </div>
    )
  }
}

function stripExtension(filename) {
  return filename.replace(/\.[^/.]+$/, '')
}
