import React from 'react'
import ReactS3Uploader from 'react-s3-uploader'

export default function Uploader(props) {
  return <ReactS3Uploader {...props} signingUrl={signingUrl()} />
}

function signingUrl() {
  const { hostname, port, protocol } = location
  const base = port ? `${hostname}:${port}` : hostname

  return `${protocol}//${base}/api/uploads/sign`
}
