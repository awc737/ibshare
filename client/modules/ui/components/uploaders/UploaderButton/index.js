import React, { Component } from 'react'
import classnames from 'classnames'
import { merge } from 'ramda'

import { Button, LoadingSpinner } from 'modules/ui'

import styles from './styles.scss'

export default class UploaderButton extends Component {
  static defaultProps = {
    icon: 'description'
  }

  activeIcon() {
    const { complete, icon } = this.props

    return complete ? 'check' : icon
  }

  render() {
    const { className, loading, small } = this.props

    const classNames = classnames(className, {
      [styles.small]: small,
      [styles.large]: !small
    })

    const spinnerProps = loading
      ? { icon: '', children: <LoadingSpinner /> }
      : { icon: this.activeIcon() }

    const props = {
      className: classNames,
      floating: Boolean(small),
      ...merge(this.props, spinnerProps)
    }

    return (
      <Button {...props} />
    )
  }
}
