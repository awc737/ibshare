import React from 'react'

import { Content } from 'modules/layout'
import { Button } from 'modules/ui'

import styles from './styles.scss'

export default function FieldManager({ addValue, fieldLabel }) {
  return (
    <Content className='FieldManager small-12 medium-6 large-4 text-center'>
      <Button
          className={styles.button}
          icon='add'
          onClick={addValue} />
      <label className={styles.fieldLabel}>{fieldLabel}</label>
    </Content>
  )
}
