import React from 'react'

import styles from './styles.scss'

export default function Input(props) {
  const { label, type = 'text', inputRef } = props

  return (
    <div>
      {label && <label className={styles.label}>{label}</label>}

      <input
          {...props}
          className={styles.input}
          ref={inputRef}
          type={type} />
    </div>
  )
}
