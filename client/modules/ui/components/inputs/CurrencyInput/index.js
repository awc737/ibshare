import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import numbro from 'numbro'
import { take } from 'ramda'

import { Input } from 'modules/ui'
import { allowNumbers } from 'utils'

export default class CurrencyInput extends Component {
  constructor() {
    super()

    this.captureState = this.captureState.bind(this)
    this.handleChange = this.handleChange.bind(this)

    this.state = { selectionStart: 0 }
  }

  componentDidMount() {
    this.input = ReactDOM.findDOMNode(this).querySelector('input')
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.value !== this.props.value
  }

  componentDidUpdate(previousProps, previousState) {
    this.setSelectionRange(previousState, this.state)
  }

  handleChange(value) {
    this.captureState()
    this.props.onChange(formatCurrency(value))
  }

  captureState() {
    const { selectionStart, value } = this.input

    // eslint-disable-next-line react/no-set-state
    this.setState({ selectionStart, value: formatCurrency(value) })
  }

  setSelectionRange(oldState, newState) {
    const offset =
      specialCharCount(newState.value) - specialCharCount(oldState.value)

    const selectionStart = newState.selectionStart + offset

    this.input.setSelectionRange(selectionStart, selectionStart)
  }

  render() {
    return (
      <Input
          {...this.props}
          onChange={this.handleChange}
          onKeyPress={allowNumbers} />
    )
  }
}

function specialCharCount(string = '') {
  return string.replace(/[^,\$]/g, '').length
}

function formatCurrency(string) {
  const number = numbro().unformat(take(18, string))

  return number ? numbro(number).formatCurrency('$0,0') : ''
}
