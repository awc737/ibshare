import React from 'react'

import styles from './styles.scss'

export default function TextArea({ label, ...props }) {
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <textarea {...props} className={styles.textarea} />
    </div>
  )
}
