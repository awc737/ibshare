import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Input } from 'modules/ui'
import { allowDecimalNumbers } from 'utils'

/* eslint-disable react/no-set-state */
export default class PercentInput extends Component {
  constructor() {
    super()

    this.capturePosition = this.capturePosition.bind(this)
    this.handleChange = this.handleChange.bind(this)

    this.state = { selectionStart: 0 }
  }

  componentDidMount() {
    this.input = ReactDOM.findDOMNode(this).querySelector('input')
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.value !== this.props.value
  }

  componentDidUpdate(previousProps, previousState) {
    this.setSelectionRange(previousState, this.state)
  }

  handleChange(value) {
    this.capturePosition()
    this.props.onChange(formatPercent(value))
  }

  capturePosition() {
    const { selectionStart } = this.input

    this.setState({ selectionStart })
  }

  setSelectionRange(oldState, newState) {
    const selectionStart = newState.selectionStart

    this.input.setSelectionRange(selectionStart, selectionStart)
  }

  render() {
    return (
      <Input
          {...this.props}
          onChange={this.handleChange}
          onKeyPress={allowDecimalNumbers} />
    )
  }
}

function formatPercent(string) {
  return string.replace(/%/g, '') + '%'
}
