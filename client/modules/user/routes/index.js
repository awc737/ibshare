import React from 'react'
import { IndexRedirect, Route } from 'react-router'

import Auth from '../containers/Auth'
import Login from '../containers/Login'
import Registration from '../containers/Registration'
import ResetPassword from '../containers/ResetPassword'
import UpdatePassword from '../containers/UpdatePassword'

export default (
  <Route name='user' path='user' component={Auth}>
    <IndexRedirect to='sign_up' />

    <Route
        name='reset_password'
        path='reset_password'
        component={UpdatePassword} />
    <Route
        name='forgot_password'
        path='forgot_password'
        component={ResetPassword} />

    <Route name='sign_up' path='sign_up' component={Registration} />
    <Route name='sign_in' path='sign_in' component={Login} />
  </Route>
)
