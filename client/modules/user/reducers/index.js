import { compose } from 'ramda'

import ActionTypes from '../constants/ActionTypes'
import { createReducer } from 'utils'

const initialState = {
  currentUser: {},
  welcomeMessage: false,
  passwordResetSent: false
}

const handlers = {
  [ActionTypes.CREATE_USER_SUCCESS]: compose(welcomeUser, setUser),
  [ActionTypes.DISMISS_WELCOME]: dismissWelcome,
  [ActionTypes.FETCH_USER_SUCCESS]: setUser,
  [ActionTypes.RESET_PASSWORD_REQUEST]: clearPasswordReset,
  [ActionTypes.RESET_PASSWORD_SUCCESS]: passwordResetSent,
  [ActionTypes.SIGN_IN_SUCCESS]: setUser,
  [ActionTypes.UPDATE_PASSWORD_SUCCESS]: setUser
}

export default createReducer(initialState, handlers)

function setUser(state, { payload }) {
  return { ...state, currentUser: payload }
}

function welcomeUser(state) {
  return { ...state, welcomeMessage: true }
}

function dismissWelcome(state) {
  return { ...state, welcomeMessage: false }
}

function passwordResetSent(state) {
  return { ...state, passwordResetSent: true }
}

function clearPasswordReset(state) {
  return { ...state, passwordResetSent: false }
}
