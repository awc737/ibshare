export const currentUser = state => state.app.user.currentUser
export const welcomeMessageActive = state => state.app.user.welcomeMessage
export const passwordResetSent = state => state.app.user.passwordResetSent
export const token = state =>
  state.routing.locationBeforeTransitions.query.token
