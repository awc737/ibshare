import { validate } from './registration'

const fields = [
  'password',
  'passwordConfirmation',
  'resetPasswordToken'
]

const initialValues = {}

export function config(name) {
  return { fields, initialValues, validate, ...{ form: name } }
}
