const fields = [
  'email'
]

const initialValues = {}

export function config(name) {
  return { fields, initialValues, ...{ form: name } }
}
