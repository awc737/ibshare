import { head, isNil, map, reject, toPairs } from 'ramda'

const fields = [
  'acceptedTerms',
  'passwordConfirmation',
  'email',
  'firstName',
  'lastName',
  'password'
]

const initialValues = {}

export function config(name) {
  return { fields, initialValues, validate, ...{ form: name } }
}

export function validate(values) {
  if (!values.password) return {}

  const requirements = {
    'requires at least one lowercase letter': /[a-z]/,
    'requires at least one uppercase letter': /[A-Z]/,
    'requires at least one digit': /\d/,
    'requires at least 8 characters': /.{8,}/
  }

  const check = ([error, regex]) => values.password.match(regex) ? null : error
  const errors = reject(isNil, map(check, toPairs(requirements)))

  return { password: head(errors) }
}
