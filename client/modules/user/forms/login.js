const fields = [
  'email',
  'password'
]

const initialValues = {}

export function config(name) {
  return { fields, initialValues, ...{ form: name } }
}
