import React from 'react'

import { Snackbar } from 'modules/ui'

export default function WelcomeMessage({ active, ...props }) {
  const { handleClick, handleTimeout } = props

  const message =
    'Welcome to Infobrij!  ' +
    'Create a new project or find investment opportunities above'

  return (
    <Snackbar
        action='dismiss'
        active={active}
        label={message}
        onClick={handleClick}
        onTimeout={handleTimeout}
        timeout={5000}
        type='cancel' />
  )
}
