import React from 'react'

import { logo } from 'modules/app'

import styles from './styles.scss'

export default function Form({ children, onSubmit, title }) {
  return (
    <form className={styles.root} onSubmit={onSubmit}>
      <div className={styles.container}>
        <img className={styles.logo} src={logo} />

        <h1 className={styles.title}>
          {title}
        </h1>

        {children}
      </div>
    </form>
  )
}
