import React from 'react'
import { Link } from 'react-router-named-routes'

import { Input } from 'modules/ui'
import Form from '../Form'
import styles from '../styles.scss'

export default function UpdatePassword(props) {
  const {
    fields,
    handleSubmit,
    submitting
  } = props
  const { password, passwordConfirmation } = fields

  return (
    <Form title='Update Password' onSubmit={handleSubmit}>
      <Input
          {...password}
          icon='lock'
          label='Password'
          type='password' />

      <Input
          {...passwordConfirmation}
          icon='lock'
          label='Confirm Password'
          type='password' />

      <button
          className={styles.continueButton}
          disabled={submitting}
          onClick={handleSubmit}>
        Continue
      </button>

      <Link className={styles.loginLink} to='sign_in'>
        Log In
      </Link>
    </Form>
  )
}
