import { Component } from 'react'

import { navigateTo } from 'utils'

export default class Auth extends Component {
  componentWillMount() {
    this.props.fetchUser().then(({ payload }) => {
      if (payload.id) navigateTo('root')
    })
  }

  render() {
    return this.props.children
  }
}
