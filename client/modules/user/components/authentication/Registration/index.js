import React from 'react'
import { Link } from 'react-router-named-routes'
import classnames from 'classnames'

import { Terms } from 'modules/app'
import { Checkbox, Input } from 'modules/ui'
import Form from '../Form'

import styles from '../styles.scss'

export default function Registration(props) {
  const { fields, handleSubmit } = props
  const {
    acceptedTerms,
    firstName,
    lastName,
    email,
    password,
    passwordConfirmation
  } = fields

  return (
    <Form title='Create an Account' onSubmit={handleSubmit}>
      <NameInput {...firstName} icon='person' label='First Name' />
      <NameInput {...lastName} label='Last Name' />

      <Input
          {...email}
          error={email.error}
          icon='email'
          label='Email'
          type='email' />

      <Input
          {...password}
          error={password.error}
          icon='lock'
          label='Password'
          type='password' />

      <Input
          {...passwordConfirmation}
          error={passwordConfirmation.error}
          icon='lock'
          label='Confirm Password'
          type='password' />

      <TermsCheckbox {...acceptedTerms} />
      <GetStarted {...props} />

      <Link className={styles.loginLink} to='sign_in'>
        Log In
      </Link>
    </Form>
  )
}

function NameInput(props) {
  return (
    <div className={styles.nameInput}>
      <Input {...props} />
    </div>
  )
}

function TermsCheckbox(props) {
  const { checked } = props

  const classNames = classnames(styles.checkbox, {
    [styles.unchecked]: !checked
  })

  return (
    <div className={styles.terms}>
      <label className={styles.termsLabel}>
        <Checkbox {...props} className={classNames} />
        <span>Accept our</span>
      </label>

      <Terms />
    </div>
  )
}

function GetStarted({ fields: { acceptedTerms }, submitting, handleSubmit }) {
  return (
    <button
        className={styles.continueButton}
        disabled={!acceptedTerms.value || submitting}
        onClick={handleSubmit}>
      GET STARTED
    </button>
  )
}
