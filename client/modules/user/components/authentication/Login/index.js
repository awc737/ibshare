import React from 'react'
import { Link } from 'react-router-named-routes'

import { Input } from 'modules/ui'
import Form from '../Form'

import styles from '../styles.scss'

export default function Login(props) {
  const { error, fields, handleSubmit, submitting } = props
  const { email, password } = fields

  return (
    <Form title='Log In' onSubmit={handleSubmit}>
      <Input {...email} icon='email' label='Email' type='email' />
      <Input {...password} icon='lock' label='Password' type='password' />

      {error &&
        <div className={styles.errorNotification}>
          Invalid email or password.
        </div>}

      <button
          className={styles.continueButton}
          disabled={submitting}
          onClick={handleSubmit}>
        Continue
      </button>

      <Link className={styles.loginLink} to='sign_up'>
        Sign Up
      </Link>

      <Link className={styles.passwordLink} to='forgot_password'>
        Forgot Password?
      </Link>
    </Form>
  )
}
