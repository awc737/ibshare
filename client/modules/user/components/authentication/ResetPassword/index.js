import React from 'react'
import { Link } from 'react-router-named-routes'

import { Input } from 'modules/ui'
import Form from '../Form'
import styles from '../styles.scss'

export default function ResetPassword(props) {
  const {
    passwordResetSent,
    fields: { email },
    handleSubmit,
    submitting
  } = props

  return (
    <Form title='Reset Password' onSubmit={handleSubmit}>
      <Input
          {...email}
          error={email.error}
          icon='email'
          label='Email'
          type='email' />

      {passwordResetSent &&
        <div className={styles.successNotification}>
          Password reset email sent.
        </div>}

      <button
          className={styles.continueButton}
          disabled={submitting}
          onClick={handleSubmit}>
        Continue
      </button>

      <Link className={styles.loginLink} to='sign_in'>
        Log In
      </Link>
    </Form>
  )
}
