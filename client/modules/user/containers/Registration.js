import { reduxForm } from 'redux-form'

import { registerUser } from '../actions'
import { Registration } from '../components/authentication'
import { config } from '../forms/registration'
import { formApiAdapter } from 'utils'

const name = 'registration'

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: formApiAdapter(dispatch, registerUser)
  }
}

export default reduxForm(
  config(name),
  null,
  mapDispatchToProps
)(Registration)
