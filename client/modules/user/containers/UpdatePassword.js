import { reduxForm } from 'redux-form'

import { updatePassword } from '../actions'
import { UpdatePassword } from '../components/authentication'
import { config } from '../forms/updatePassword'
import { token } from '../selectors'
import { formApiAdapter } from 'utils'

const name = 'updatePassword'

function mapStateToProps(state) {
  return {
    initialValues: {
      resetPasswordToken: token(state)
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: formApiAdapter(dispatch, updatePassword)
  }
}

export default reduxForm(
  config(name),
  mapStateToProps,
  mapDispatchToProps
)(UpdatePassword)
