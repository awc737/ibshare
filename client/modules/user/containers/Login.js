import { reduxForm } from 'redux-form'

import { signInUser } from '../actions'
import { Login } from '../components/authentication'
import { config } from '../forms/login'
import { formApiAdapter } from 'utils'

const name = 'login'

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: formApiAdapter(dispatch, signInUser)
  }
}

export default reduxForm(
  config(name),
  null,
  mapDispatchToProps
)(Login)
