import { connect } from 'react-redux'

import { Auth } from '../components/authentication'
import { fetchUser } from '../actions'

export default connect(null, { fetchUser })(Auth)
