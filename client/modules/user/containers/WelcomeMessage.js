import { connect } from 'react-redux'

import WelcomeMessage from '../components/WelcomeMessage'
import { dismissWelcome } from '../actions'
import { welcomeMessageActive } from '../selectors'

const callbacks = {
  handleClick: dismissWelcome,
  handleTimeout: dismissWelcome
}

function mapStateToProps(state) {
  return { active: welcomeMessageActive(state) }
}

export default connect(mapStateToProps, callbacks)(WelcomeMessage)
