import { reduxForm } from 'redux-form'

import { resetPassword } from '../actions'
import { ResetPassword } from '../components/authentication'
import { config } from '../forms/resetPassword'
import { passwordResetSent } from '../selectors'
import { formApiAdapter } from 'utils'

const name = 'resetPassword'

function mapStateToProps(state) {
  return {
    passwordResetSent: passwordResetSent(state)
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: formApiAdapter(dispatch, resetPassword)
  }
}

export default reduxForm(
  config(name),
  mapStateToProps,
  mapDispatchToProps
)(ResetPassword)
