import ActionTypes from '../constants/ActionTypes'
import { callApi, navigateTo } from 'utils'

export function dismissWelcome() {
  return { type: ActionTypes.DISMISS_WELCOME }
}

export function registerUser(payload) {
  const onSuccess = () => navigateTo('root')

  return callApi({
    endpoint: '/api/users',
    method: 'POST',
    body: { user: payload },
    types: [
      ActionTypes.CREATE_USER_REQUEST,
      ActionTypes.CREATE_USER_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  }, { onSuccess })
}

export function signInUser(payload) {
  const onSuccess = () => navigateTo('root')

  return callApi({
    endpoint: '/api/users/sign_in',
    method: 'POST',
    body: { user: payload },
    types: [
      ActionTypes.SIGN_IN_REQUEST,
      ActionTypes.SIGN_IN_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  }, { onSuccess })
}

export function fetchUser() {
  return callApi({
    endpoint: '/api/current_user',
    types: [
      ActionTypes.FETCH_USER_REQUEST,
      ActionTypes.FETCH_USER_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  })
}

export function signOut() {
  const onSuccess = () => navigateTo('sign_in')

  return callApi({
    endpoint: '/api/users/sign_out',
    method: 'DELETE',
    types: [
      ActionTypes.SIGN_OUT_REQUEST,
      ActionTypes.SIGN_OUT_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  }, { onSuccess })
}

export function resetPassword(payload) {
  return callApi({
    endpoint: '/api/users/password',
    method: 'POST',
    body: { user: payload },
    types: [
      ActionTypes.RESET_PASSWORD_REQUEST,
      ActionTypes.RESET_PASSWORD_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  })
}

export function updatePassword(payload) {
  const onSuccess = () => navigateTo('root')

  return callApi({
    endpoint: '/api/users/password',
    method: 'PUT',
    body: { user: payload },
    types: [
      ActionTypes.UPDATE_PASSWORD_REQUEST,
      ActionTypes.UPDATE_PASSWORD_SUCCESS,
      ActionTypes.API_FAILURE
    ]
  }, { onSuccess })
}
