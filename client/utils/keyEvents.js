import { contains, props, values } from 'ramda'

const numbers = {
  0: 48,
  1: 49,
  2: 50,
  3: 51,
  4: 52,
  5: 53,
  6: 54,
  7: 55,
  8: 56,
  9: 57
}

const keys = {
  '-': 45,
  '.': 46,
  ...numbers,
  b: 98,
  e: 101,
  k: 107,
  m: 109,
  t: 116
}

export function allowNumbers(event) {
  const pressed = key(event)

  if (!contains(pressed, values(numbers))) event.preventDefault()
}

export function allowDecimalNumbers(event) {
  if (invalidDecimal(event)) event.preventDefault()
}

export function preventKeys(...blocked) {
  return event => {
    const invalid = props(blocked, keys)
    const pressed = key(event)

    if (contains(pressed, invalid)) event.preventDefault()
  }
}

export function keyPressed(name, event) {
  return key(event) === keys[name]
}

function key(event) {
  return event.charCode || event.keyCode
}

function invalidDecimal(event) {
  const pressed = key(event)
  const validKeys = [...values(numbers), keys['.']]

  return !contains(pressed, validKeys) || multipleDecimals(event, pressed)
}

function multipleDecimals(event, pressed) {
  return pressed === keys['.'] && contains('.', event.target.value)
}
