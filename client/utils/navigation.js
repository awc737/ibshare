import { NamedURLResolver } from 'react-router-named-routes'
import { browserHistory } from 'react-router'

export function navigateTo(routeName, params = {}) {
  return browserHistory.push(namedRoute(routeName, params))
}

function namedRoute(name, params) {
  return NamedURLResolver.resolve(name, params)
}
