export default function unformatCurrency(value) {
  return value && parseFloat(value.replace(/[\$,]/g, ''))
}
