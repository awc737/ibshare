import {
  concat,
  defaultTo,
  join,
  pipe,
  reverse,
  splitEvery,
  toString
} from 'ramda'

export default pipe(
  defaultTo(0),
  parseFloat,
  toString,
  reverse,
  splitEvery(3),
  join(','),
  reverse,
  concat('$')
)
