import { expect } from 'chai'

import createActionTypes from '../createActionTypes'

describe('createActionTypes', () => {
  it('creates a map with namespeced values', () => {
    const namespace = 'module'
    const actionList = ['FOO', 'BAR', 'BAZ']

    const result = createActionTypes(namespace, actionList)

    expect(result).to.eql({
      FOO: '~module/FOO',
      BAR: '~module/BAR',
      BAZ: '~module/BAZ'
    })
  })
})
