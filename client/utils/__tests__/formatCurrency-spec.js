import { expect } from 'chai'

import formatCurrency from '../formatCurrency'

describe('formatCurrency', () => {
  it('prepends a dollar sign', () => {
    expect(formatCurrency(123)).to.eq('$123')
  })

  it('adds commas where necessary', () => {
    expect(formatCurrency(1234)).to.eq('$1,234')
    expect(formatCurrency(123456789)).to.eq('$123,456,789')
  })
})
