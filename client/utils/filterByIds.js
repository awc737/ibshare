import { map } from 'ramda'

export default function filterByIds(ids = [], collection = []) {
  return map(id => collection[id], ids)
}
