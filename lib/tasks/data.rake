# frozen_string_literal: true
namespace :data do
  desc 'set the default sort order of media'
  task set_uploads_order: :environment do
    Project.all.each do |pr|
      pr.media.each_with_index do |m, i|
        m.set_list_position(i)
        m.save!
      end
    end
  end
end
