const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const SassLintPlugin = require('sasslint-webpack-plugin')
const autoprefixer = require('autoprefixer')

const extractCSS = new ExtractTextPlugin('vendor.css', {
  allChunks: true
})

module.exports = {
  entry: {
    client: ['babel-polyfill', path.resolve(__dirname, '../client/index.js')]
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel']
      }, {
        test: /\.css$/,
        loader: extractCSS.extract('css')
      }, {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
        loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
      }, {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ],
    preLoaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'eslint-loader'
    }]
  },
  postcss: function () {
    return [autoprefixer];
  },
  output: {
    path: path.resolve(__dirname, '../public/build'),
    filename: '[name].js'
  },
  plugins: [
    extractCSS,
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.ProvidePlugin({
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
      jQuery: 'jquery',
      $: 'expose?$!jquery'
    }),
    new SassLintPlugin({
      configFile: path.resolve(__dirname, '../.sass-lint.yml'),
      glob: 'client/**/*.scss'
    })
  ],
  resolve: {
    root: path.resolve(__dirname, '../client'),
    extensions: ['', '.js', '.scss']
  },
  toolbox: {
    theme: 'client/styles/theme.scss'
  }
}
