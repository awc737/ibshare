# Infobrij

The Infobrij platform. See the [Project Glossary](docs/glossary.md).

## Setup

Run this once after listed dependencies have been installed:

```bash
bin/setup
```

### Dependencies

#### Ruby

Use the version specified in the [Gemfile](Gemfile) and make sure you have the
latest bundler with:

```bash
gem install bundler
```

[rbenv](https://github.com/rbenv/rbenv) is a good option for managing ruby
versions.

#### Node

Use the node and npm versions specified in the [package.json](package.json).

[nvm](https://github.com/creationix/nvm) is a good option for managing node
versions.

#### PhantomJS

2.1.1 or higher is required for running feature tests.

It can be installed with [Homebrew](http://brew.sh/) on a Mac:

```bash
brew install phantomjs
```

#### Redis

Redis is used by sidekiq for storing delayed jobs.

```bash
brew install redis
```

## Development

In three different terminal sessions, run:

```bash
redis-server
```

```bash
foreman start
```

```bash
npm run dev
```

Then visit: [http://localhost:3000](http://localhost:3000)

### Chrome Extensions

These are huge aids with front end development:

- [React Dev Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

- [Redux Dev Tools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd)

### Delayed Jobs

You can view the sidekiq web interface at:
[http://localhost:3000/sidekiq](http://localhost:3000/sidekiq)

This requires an admin user account.

### Mailers

To locally inspect outbound mail, run mailcatcher and click the link it provides for a web interface.

```bash
mailcatcher
```

## Testing

### All Tests

Run all lint checks and tests:

```bash
rake
```

### Watch / Auto Run

You can watch for changes and have relevant tests run automatically.

In two different terminal sessions, run:

```bash
bundle exec guard
```

```bash
npm run test:watch
```

### Single Run

Run rspec and feature tests with:

```bash
bundle exec rspec
```

Run mocha unit tests with:

```bash
npm test
```

### Debugging

You can run javascript tests in the browser for dev tool debugging with:

```bash
npm run test:debug
```

### Just Lint

You can lint your ruby and javascript with:

```bash
rubocop
```

```bash
npm run lint
```


## Troubleshooting

### Dependencies

Easy to forget when pulling down updates.

```bash
bundle install
```

```bash
npm install
```

### Database

```bash
rake db:migrate db:test:prepare
```

Or for a complete reset, close all database connections and try:

```bash
rake db:reset
```

### Rebuilding

Importing new static assets such as images or css usually requires a rebuild:

```bash
npm run build
```
